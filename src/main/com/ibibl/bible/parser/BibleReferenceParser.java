// Generated from BibleReference.g4 by ANTLR 4.5
package com.ibibl.bible.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BibleReferenceParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NUM=1, DOT=2, GUION=3, DOSPUNTOS=4, SPACE=5, PUNTOYCOMA=6, SEPARATOR=7, 
		COMA=8, AMOS=9, FIRSTCHRONICLES=10, SECONDCHRONICLES=11, DANIEL=12, DEUTERONOMY=13, 
		ECCLESIASTES=14, ESTHER=15, EXODUS=16, EZEKIEL=17, EZRA=18, GENESIS=19, 
		HABAKKUK=20, HAGGAI=21, HOSEA=22, ISAIAH=23, JEREMIAH=24, JOB=25, JOEL=26, 
		JONAH=27, JOSHUA=28, JUDGES=29, FIRSTKINGS=30, SECONDKINGS=31, LAMENTATIONS=32, 
		LEVITICUS=33, MALACHI=34, MICAH=35, NAHUM=36, NEHEMIAH=37, NUMBERS=38, 
		OBADIAH=39, PROVERBS=40, PSALMS=41, RUTH=42, FIRSTSAMUEL=43, SECONDSAMUEL=44, 
		SONGOFSOLOMON=45, ZECHARIAH=46, ZEPHANIAH=47, ACTS=48, REVELATION=49, 
		COLOSSIANS=50, FIRSTCORINTHIANS=51, SECONDCORINTHIANS=52, EPHESIANS=53, 
		GALATIANS=54, HEBREWS=55, JAMES=56, JOHN=57, FIRSTJOHN=58, SECONDJOHN=59, 
		THIRDJOHN=60, JUDE=61, LUKE=62, MARK=63, MATTHEW=64, FIRSTPETER=65, SECONDPETER=66, 
		PHILEMON=67, PHILIPPIANS=68, ROMANS=69, FIRSTTHESSALONIANS=70, SECONDTHESSALONIANS=71, 
		FIRSTTIMOTHY=72, SECONDTIMOTHY=73, TITUS=74;
	public static final int
		RULE_ref = 0, RULE_book = 1, RULE_refList = 2, RULE_chapterRef = 3, RULE_simpleRef = 4, 
		RULE_chapterOnly = 5, RULE_chapterVerse = 6, RULE_chapterVerseRange = 7, 
		RULE_rangeRef = 8, RULE_chapterRange = 9, RULE_chapterRangeVerse = 10, 
		RULE_multipleVerseRef = 11, RULE_verseExp = 12;
	public static final String[] ruleNames = {
		"ref", "book", "refList", "chapterRef", "simpleRef", "chapterOnly", "chapterVerse", 
		"chapterVerseRange", "rangeRef", "chapterRange", "chapterRangeVerse", 
		"multipleVerseRef", "verseExp"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, "'-'", null, null, "';'", null, "','"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "NUM", "DOT", "GUION", "DOSPUNTOS", "SPACE", "PUNTOYCOMA", "SEPARATOR", 
		"COMA", "AMOS", "FIRSTCHRONICLES", "SECONDCHRONICLES", "DANIEL", "DEUTERONOMY", 
		"ECCLESIASTES", "ESTHER", "EXODUS", "EZEKIEL", "EZRA", "GENESIS", "HABAKKUK", 
		"HAGGAI", "HOSEA", "ISAIAH", "JEREMIAH", "JOB", "JOEL", "JONAH", "JOSHUA", 
		"JUDGES", "FIRSTKINGS", "SECONDKINGS", "LAMENTATIONS", "LEVITICUS", "MALACHI", 
		"MICAH", "NAHUM", "NEHEMIAH", "NUMBERS", "OBADIAH", "PROVERBS", "PSALMS", 
		"RUTH", "FIRSTSAMUEL", "SECONDSAMUEL", "SONGOFSOLOMON", "ZECHARIAH", "ZEPHANIAH", 
		"ACTS", "REVELATION", "COLOSSIANS", "FIRSTCORINTHIANS", "SECONDCORINTHIANS", 
		"EPHESIANS", "GALATIANS", "HEBREWS", "JAMES", "JOHN", "FIRSTJOHN", "SECONDJOHN", 
		"THIRDJOHN", "JUDE", "LUKE", "MARK", "MATTHEW", "FIRSTPETER", "SECONDPETER", 
		"PHILEMON", "PHILIPPIANS", "ROMANS", "FIRSTTHESSALONIANS", "SECONDTHESSALONIANS", 
		"FIRSTTIMOTHY", "SECONDTIMOTHY", "TITUS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BibleReference.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BibleReferenceParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class RefContext extends ParserRuleContext {
		public BookContext book() {
			return getRuleContext(BookContext.class,0);
		}
		public TerminalNode SPACE() { return getToken(BibleReferenceParser.SPACE, 0); }
		public RefListContext refList() {
			return getRuleContext(RefListContext.class,0);
		}
		public TerminalNode DOT() { return getToken(BibleReferenceParser.DOT, 0); }
		public RefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ref; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitRef(this);
		}
	}

	public final RefContext ref() throws RecognitionException {
		RefContext _localctx = new RefContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_ref);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26);
			book();
			setState(28);
			_la = _input.LA(1);
			if (_la==DOT) {
				{
				setState(27);
				match(DOT);
				}
			}

			setState(30);
			match(SPACE);
			setState(31);
			refList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BookContext extends ParserRuleContext {
		public TerminalNode AMOS() { return getToken(BibleReferenceParser.AMOS, 0); }
		public TerminalNode FIRSTCHRONICLES() { return getToken(BibleReferenceParser.FIRSTCHRONICLES, 0); }
		public TerminalNode SECONDCHRONICLES() { return getToken(BibleReferenceParser.SECONDCHRONICLES, 0); }
		public TerminalNode DANIEL() { return getToken(BibleReferenceParser.DANIEL, 0); }
		public TerminalNode DEUTERONOMY() { return getToken(BibleReferenceParser.DEUTERONOMY, 0); }
		public TerminalNode ECCLESIASTES() { return getToken(BibleReferenceParser.ECCLESIASTES, 0); }
		public TerminalNode ESTHER() { return getToken(BibleReferenceParser.ESTHER, 0); }
		public TerminalNode EXODUS() { return getToken(BibleReferenceParser.EXODUS, 0); }
		public TerminalNode EZEKIEL() { return getToken(BibleReferenceParser.EZEKIEL, 0); }
		public TerminalNode EZRA() { return getToken(BibleReferenceParser.EZRA, 0); }
		public TerminalNode GENESIS() { return getToken(BibleReferenceParser.GENESIS, 0); }
		public TerminalNode HABAKKUK() { return getToken(BibleReferenceParser.HABAKKUK, 0); }
		public TerminalNode HAGGAI() { return getToken(BibleReferenceParser.HAGGAI, 0); }
		public TerminalNode HOSEA() { return getToken(BibleReferenceParser.HOSEA, 0); }
		public TerminalNode ISAIAH() { return getToken(BibleReferenceParser.ISAIAH, 0); }
		public TerminalNode JEREMIAH() { return getToken(BibleReferenceParser.JEREMIAH, 0); }
		public TerminalNode JOB() { return getToken(BibleReferenceParser.JOB, 0); }
		public TerminalNode JOEL() { return getToken(BibleReferenceParser.JOEL, 0); }
		public TerminalNode JONAH() { return getToken(BibleReferenceParser.JONAH, 0); }
		public TerminalNode JOSHUA() { return getToken(BibleReferenceParser.JOSHUA, 0); }
		public TerminalNode JUDGES() { return getToken(BibleReferenceParser.JUDGES, 0); }
		public TerminalNode FIRSTKINGS() { return getToken(BibleReferenceParser.FIRSTKINGS, 0); }
		public TerminalNode SECONDKINGS() { return getToken(BibleReferenceParser.SECONDKINGS, 0); }
		public TerminalNode LAMENTATIONS() { return getToken(BibleReferenceParser.LAMENTATIONS, 0); }
		public TerminalNode LEVITICUS() { return getToken(BibleReferenceParser.LEVITICUS, 0); }
		public TerminalNode MALACHI() { return getToken(BibleReferenceParser.MALACHI, 0); }
		public TerminalNode MICAH() { return getToken(BibleReferenceParser.MICAH, 0); }
		public TerminalNode NAHUM() { return getToken(BibleReferenceParser.NAHUM, 0); }
		public TerminalNode NEHEMIAH() { return getToken(BibleReferenceParser.NEHEMIAH, 0); }
		public TerminalNode NUMBERS() { return getToken(BibleReferenceParser.NUMBERS, 0); }
		public TerminalNode OBADIAH() { return getToken(BibleReferenceParser.OBADIAH, 0); }
		public TerminalNode PROVERBS() { return getToken(BibleReferenceParser.PROVERBS, 0); }
		public TerminalNode PSALMS() { return getToken(BibleReferenceParser.PSALMS, 0); }
		public TerminalNode RUTH() { return getToken(BibleReferenceParser.RUTH, 0); }
		public TerminalNode FIRSTSAMUEL() { return getToken(BibleReferenceParser.FIRSTSAMUEL, 0); }
		public TerminalNode SECONDSAMUEL() { return getToken(BibleReferenceParser.SECONDSAMUEL, 0); }
		public TerminalNode SONGOFSOLOMON() { return getToken(BibleReferenceParser.SONGOFSOLOMON, 0); }
		public TerminalNode ZECHARIAH() { return getToken(BibleReferenceParser.ZECHARIAH, 0); }
		public TerminalNode ZEPHANIAH() { return getToken(BibleReferenceParser.ZEPHANIAH, 0); }
		public TerminalNode ACTS() { return getToken(BibleReferenceParser.ACTS, 0); }
		public TerminalNode REVELATION() { return getToken(BibleReferenceParser.REVELATION, 0); }
		public TerminalNode COLOSSIANS() { return getToken(BibleReferenceParser.COLOSSIANS, 0); }
		public TerminalNode FIRSTCORINTHIANS() { return getToken(BibleReferenceParser.FIRSTCORINTHIANS, 0); }
		public TerminalNode SECONDCORINTHIANS() { return getToken(BibleReferenceParser.SECONDCORINTHIANS, 0); }
		public TerminalNode EPHESIANS() { return getToken(BibleReferenceParser.EPHESIANS, 0); }
		public TerminalNode GALATIANS() { return getToken(BibleReferenceParser.GALATIANS, 0); }
		public TerminalNode HEBREWS() { return getToken(BibleReferenceParser.HEBREWS, 0); }
		public TerminalNode JAMES() { return getToken(BibleReferenceParser.JAMES, 0); }
		public TerminalNode JOHN() { return getToken(BibleReferenceParser.JOHN, 0); }
		public TerminalNode FIRSTJOHN() { return getToken(BibleReferenceParser.FIRSTJOHN, 0); }
		public TerminalNode SECONDJOHN() { return getToken(BibleReferenceParser.SECONDJOHN, 0); }
		public TerminalNode THIRDJOHN() { return getToken(BibleReferenceParser.THIRDJOHN, 0); }
		public TerminalNode JUDE() { return getToken(BibleReferenceParser.JUDE, 0); }
		public TerminalNode LUKE() { return getToken(BibleReferenceParser.LUKE, 0); }
		public TerminalNode MARK() { return getToken(BibleReferenceParser.MARK, 0); }
		public TerminalNode MATTHEW() { return getToken(BibleReferenceParser.MATTHEW, 0); }
		public TerminalNode FIRSTPETER() { return getToken(BibleReferenceParser.FIRSTPETER, 0); }
		public TerminalNode SECONDPETER() { return getToken(BibleReferenceParser.SECONDPETER, 0); }
		public TerminalNode PHILEMON() { return getToken(BibleReferenceParser.PHILEMON, 0); }
		public TerminalNode PHILIPPIANS() { return getToken(BibleReferenceParser.PHILIPPIANS, 0); }
		public TerminalNode ROMANS() { return getToken(BibleReferenceParser.ROMANS, 0); }
		public TerminalNode FIRSTTHESSALONIANS() { return getToken(BibleReferenceParser.FIRSTTHESSALONIANS, 0); }
		public TerminalNode SECONDTHESSALONIANS() { return getToken(BibleReferenceParser.SECONDTHESSALONIANS, 0); }
		public TerminalNode FIRSTTIMOTHY() { return getToken(BibleReferenceParser.FIRSTTIMOTHY, 0); }
		public TerminalNode SECONDTIMOTHY() { return getToken(BibleReferenceParser.SECONDTIMOTHY, 0); }
		public TerminalNode TITUS() { return getToken(BibleReferenceParser.TITUS, 0); }
		public BookContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_book; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterBook(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitBook(this);
		}
	}

	public final BookContext book() throws RecognitionException {
		BookContext _localctx = new BookContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_book);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(33);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << AMOS) | (1L << FIRSTCHRONICLES) | (1L << SECONDCHRONICLES) | (1L << DANIEL) | (1L << DEUTERONOMY) | (1L << ECCLESIASTES) | (1L << ESTHER) | (1L << EXODUS) | (1L << EZEKIEL) | (1L << EZRA) | (1L << GENESIS) | (1L << HABAKKUK) | (1L << HAGGAI) | (1L << HOSEA) | (1L << ISAIAH) | (1L << JEREMIAH) | (1L << JOB) | (1L << JOEL) | (1L << JONAH) | (1L << JOSHUA) | (1L << JUDGES) | (1L << FIRSTKINGS) | (1L << SECONDKINGS) | (1L << LAMENTATIONS) | (1L << LEVITICUS) | (1L << MALACHI) | (1L << MICAH) | (1L << NAHUM) | (1L << NEHEMIAH) | (1L << NUMBERS) | (1L << OBADIAH) | (1L << PROVERBS) | (1L << PSALMS) | (1L << RUTH) | (1L << FIRSTSAMUEL) | (1L << SECONDSAMUEL) | (1L << SONGOFSOLOMON) | (1L << ZECHARIAH) | (1L << ZEPHANIAH) | (1L << ACTS) | (1L << REVELATION) | (1L << COLOSSIANS) | (1L << FIRSTCORINTHIANS) | (1L << SECONDCORINTHIANS) | (1L << EPHESIANS) | (1L << GALATIANS) | (1L << HEBREWS) | (1L << JAMES) | (1L << JOHN) | (1L << FIRSTJOHN) | (1L << SECONDJOHN) | (1L << THIRDJOHN) | (1L << JUDE) | (1L << LUKE) | (1L << MARK))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MATTHEW - 64)) | (1L << (FIRSTPETER - 64)) | (1L << (SECONDPETER - 64)) | (1L << (PHILEMON - 64)) | (1L << (PHILIPPIANS - 64)) | (1L << (ROMANS - 64)) | (1L << (FIRSTTHESSALONIANS - 64)) | (1L << (SECONDTHESSALONIANS - 64)) | (1L << (FIRSTTIMOTHY - 64)) | (1L << (SECONDTIMOTHY - 64)) | (1L << (TITUS - 64)))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RefListContext extends ParserRuleContext {
		public List<ChapterRefContext> chapterRef() {
			return getRuleContexts(ChapterRefContext.class);
		}
		public ChapterRefContext chapterRef(int i) {
			return getRuleContext(ChapterRefContext.class,i);
		}
		public List<TerminalNode> PUNTOYCOMA() { return getTokens(BibleReferenceParser.PUNTOYCOMA); }
		public TerminalNode PUNTOYCOMA(int i) {
			return getToken(BibleReferenceParser.PUNTOYCOMA, i);
		}
		public List<TerminalNode> SPACE() { return getTokens(BibleReferenceParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(BibleReferenceParser.SPACE, i);
		}
		public RefListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_refList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterRefList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitRefList(this);
		}
	}

	public final RefListContext refList() throws RecognitionException {
		RefListContext _localctx = new RefListContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_refList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(35);
			chapterRef();
			setState(46);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE || _la==PUNTOYCOMA) {
				{
				{
				setState(37);
				_la = _input.LA(1);
				if (_la==SPACE) {
					{
					setState(36);
					match(SPACE);
					}
				}

				setState(39);
				match(PUNTOYCOMA);
				setState(41);
				_la = _input.LA(1);
				if (_la==SPACE) {
					{
					setState(40);
					match(SPACE);
					}
				}

				setState(43);
				chapterRef();
				}
				}
				setState(48);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterRefContext extends ParserRuleContext {
		public SimpleRefContext simpleRef() {
			return getRuleContext(SimpleRefContext.class,0);
		}
		public RangeRefContext rangeRef() {
			return getRuleContext(RangeRefContext.class,0);
		}
		public MultipleVerseRefContext multipleVerseRef() {
			return getRuleContext(MultipleVerseRefContext.class,0);
		}
		public ChapterRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterRef(this);
		}
	}

	public final ChapterRefContext chapterRef() throws RecognitionException {
		ChapterRefContext _localctx = new ChapterRefContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_chapterRef);
		try {
			setState(52);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(49);
				simpleRef();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(50);
				rangeRef();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(51);
				multipleVerseRef();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleRefContext extends ParserRuleContext {
		public ChapterOnlyContext chapterOnly() {
			return getRuleContext(ChapterOnlyContext.class,0);
		}
		public ChapterVerseContext chapterVerse() {
			return getRuleContext(ChapterVerseContext.class,0);
		}
		public ChapterVerseRangeContext chapterVerseRange() {
			return getRuleContext(ChapterVerseRangeContext.class,0);
		}
		public SimpleRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterSimpleRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitSimpleRef(this);
		}
	}

	public final SimpleRefContext simpleRef() throws RecognitionException {
		SimpleRefContext _localctx = new SimpleRefContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_simpleRef);
		try {
			setState(57);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(54);
				chapterOnly();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(55);
				chapterVerse();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(56);
				chapterVerseRange();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterOnlyContext extends ParserRuleContext {
		public TerminalNode NUM() { return getToken(BibleReferenceParser.NUM, 0); }
		public ChapterOnlyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterOnly; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterOnly(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterOnly(this);
		}
	}

	public final ChapterOnlyContext chapterOnly() throws RecognitionException {
		ChapterOnlyContext _localctx = new ChapterOnlyContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_chapterOnly);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(59);
			match(NUM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterVerseContext extends ParserRuleContext {
		public List<TerminalNode> NUM() { return getTokens(BibleReferenceParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(BibleReferenceParser.NUM, i);
		}
		public TerminalNode DOSPUNTOS() { return getToken(BibleReferenceParser.DOSPUNTOS, 0); }
		public ChapterVerseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterVerse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterVerse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterVerse(this);
		}
	}

	public final ChapterVerseContext chapterVerse() throws RecognitionException {
		ChapterVerseContext _localctx = new ChapterVerseContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_chapterVerse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			match(NUM);
			setState(62);
			match(DOSPUNTOS);
			setState(63);
			match(NUM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterVerseRangeContext extends ParserRuleContext {
		public List<TerminalNode> NUM() { return getTokens(BibleReferenceParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(BibleReferenceParser.NUM, i);
		}
		public TerminalNode SEPARATOR() { return getToken(BibleReferenceParser.SEPARATOR, 0); }
		public TerminalNode GUION() { return getToken(BibleReferenceParser.GUION, 0); }
		public ChapterVerseRangeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterVerseRange; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterVerseRange(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterVerseRange(this);
		}
	}

	public final ChapterVerseRangeContext chapterVerseRange() throws RecognitionException {
		ChapterVerseRangeContext _localctx = new ChapterVerseRangeContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_chapterVerseRange);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(65);
			match(NUM);
			setState(66);
			match(SEPARATOR);
			setState(67);
			match(NUM);
			setState(68);
			match(GUION);
			setState(69);
			match(NUM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RangeRefContext extends ParserRuleContext {
		public ChapterRangeContext chapterRange() {
			return getRuleContext(ChapterRangeContext.class,0);
		}
		public ChapterVerseRangeContext chapterVerseRange() {
			return getRuleContext(ChapterVerseRangeContext.class,0);
		}
		public RangeRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rangeRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterRangeRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitRangeRef(this);
		}
	}

	public final RangeRefContext rangeRef() throws RecognitionException {
		RangeRefContext _localctx = new RangeRefContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_rangeRef);
		try {
			setState(73);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(71);
				chapterRange();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(72);
				chapterVerseRange();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterRangeContext extends ParserRuleContext {
		public List<TerminalNode> NUM() { return getTokens(BibleReferenceParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(BibleReferenceParser.NUM, i);
		}
		public TerminalNode GUION() { return getToken(BibleReferenceParser.GUION, 0); }
		public ChapterRangeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterRange; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterRange(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterRange(this);
		}
	}

	public final ChapterRangeContext chapterRange() throws RecognitionException {
		ChapterRangeContext _localctx = new ChapterRangeContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_chapterRange);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			match(NUM);
			setState(76);
			match(GUION);
			setState(77);
			match(NUM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterRangeVerseContext extends ParserRuleContext {
		public List<TerminalNode> NUM() { return getTokens(BibleReferenceParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(BibleReferenceParser.NUM, i);
		}
		public TerminalNode GUION() { return getToken(BibleReferenceParser.GUION, 0); }
		public List<TerminalNode> SEPARATOR() { return getTokens(BibleReferenceParser.SEPARATOR); }
		public TerminalNode SEPARATOR(int i) {
			return getToken(BibleReferenceParser.SEPARATOR, i);
		}
		public ChapterRangeVerseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterRangeVerse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterRangeVerse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterRangeVerse(this);
		}
	}

	public final ChapterRangeVerseContext chapterRangeVerse() throws RecognitionException {
		ChapterRangeVerseContext _localctx = new ChapterRangeVerseContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_chapterRangeVerse);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(79);
			match(NUM);
			setState(82);
			_la = _input.LA(1);
			if (_la==SEPARATOR) {
				{
				setState(80);
				match(SEPARATOR);
				setState(81);
				match(NUM);
				}
			}

			setState(84);
			match(GUION);
			setState(85);
			match(NUM);
			setState(86);
			match(SEPARATOR);
			setState(87);
			match(NUM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultipleVerseRefContext extends ParserRuleContext {
		public TerminalNode NUM() { return getToken(BibleReferenceParser.NUM, 0); }
		public TerminalNode SEPARATOR() { return getToken(BibleReferenceParser.SEPARATOR, 0); }
		public List<VerseExpContext> verseExp() {
			return getRuleContexts(VerseExpContext.class);
		}
		public VerseExpContext verseExp(int i) {
			return getRuleContext(VerseExpContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(BibleReferenceParser.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(BibleReferenceParser.COMA, i);
		}
		public List<TerminalNode> SPACE() { return getTokens(BibleReferenceParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(BibleReferenceParser.SPACE, i);
		}
		public MultipleVerseRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multipleVerseRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterMultipleVerseRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitMultipleVerseRef(this);
		}
	}

	public final MultipleVerseRefContext multipleVerseRef() throws RecognitionException {
		MultipleVerseRefContext _localctx = new MultipleVerseRefContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_multipleVerseRef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			match(NUM);
			setState(90);
			match(SEPARATOR);
			setState(91);
			verseExp();
			setState(97);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(92);
				match(COMA);
				setState(93);
				match(SPACE);
				setState(94);
				verseExp();
				}
				}
				setState(99);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VerseExpContext extends ParserRuleContext {
		public List<TerminalNode> NUM() { return getTokens(BibleReferenceParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(BibleReferenceParser.NUM, i);
		}
		public TerminalNode GUION() { return getToken(BibleReferenceParser.GUION, 0); }
		public VerseExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_verseExp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterVerseExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitVerseExp(this);
		}
	}

	public final VerseExpContext verseExp() throws RecognitionException {
		VerseExpContext _localctx = new VerseExpContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_verseExp);
		try {
			setState(104);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(100);
				match(NUM);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(101);
				match(NUM);
				setState(102);
				match(GUION);
				setState(103);
				match(NUM);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3Lm\4\2\t\2\4\3\t\3"+
		"\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f"+
		"\t\f\4\r\t\r\4\16\t\16\3\2\3\2\5\2\37\n\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4"+
		"\5\4(\n\4\3\4\3\4\5\4,\n\4\3\4\7\4/\n\4\f\4\16\4\62\13\4\3\5\3\5\3\5\5"+
		"\5\67\n\5\3\6\3\6\3\6\5\6<\n\6\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\n\3\n\5\nL\n\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\5\fU\n\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\7\rb\n\r\f\r\16\re\13\r\3\16"+
		"\3\16\3\16\3\16\5\16k\n\16\3\16\2\2\17\2\4\6\b\n\f\16\20\22\24\26\30\32"+
		"\2\3\3\2\13Lk\2\34\3\2\2\2\4#\3\2\2\2\6%\3\2\2\2\b\66\3\2\2\2\n;\3\2\2"+
		"\2\f=\3\2\2\2\16?\3\2\2\2\20C\3\2\2\2\22K\3\2\2\2\24M\3\2\2\2\26Q\3\2"+
		"\2\2\30[\3\2\2\2\32j\3\2\2\2\34\36\5\4\3\2\35\37\7\4\2\2\36\35\3\2\2\2"+
		"\36\37\3\2\2\2\37 \3\2\2\2 !\7\7\2\2!\"\5\6\4\2\"\3\3\2\2\2#$\t\2\2\2"+
		"$\5\3\2\2\2%\60\5\b\5\2&(\7\7\2\2\'&\3\2\2\2\'(\3\2\2\2()\3\2\2\2)+\7"+
		"\b\2\2*,\7\7\2\2+*\3\2\2\2+,\3\2\2\2,-\3\2\2\2-/\5\b\5\2.\'\3\2\2\2/\62"+
		"\3\2\2\2\60.\3\2\2\2\60\61\3\2\2\2\61\7\3\2\2\2\62\60\3\2\2\2\63\67\5"+
		"\n\6\2\64\67\5\22\n\2\65\67\5\30\r\2\66\63\3\2\2\2\66\64\3\2\2\2\66\65"+
		"\3\2\2\2\67\t\3\2\2\28<\5\f\7\29<\5\16\b\2:<\5\20\t\2;8\3\2\2\2;9\3\2"+
		"\2\2;:\3\2\2\2<\13\3\2\2\2=>\7\3\2\2>\r\3\2\2\2?@\7\3\2\2@A\7\6\2\2AB"+
		"\7\3\2\2B\17\3\2\2\2CD\7\3\2\2DE\7\t\2\2EF\7\3\2\2FG\7\5\2\2GH\7\3\2\2"+
		"H\21\3\2\2\2IL\5\24\13\2JL\5\20\t\2KI\3\2\2\2KJ\3\2\2\2L\23\3\2\2\2MN"+
		"\7\3\2\2NO\7\5\2\2OP\7\3\2\2P\25\3\2\2\2QT\7\3\2\2RS\7\t\2\2SU\7\3\2\2"+
		"TR\3\2\2\2TU\3\2\2\2UV\3\2\2\2VW\7\5\2\2WX\7\3\2\2XY\7\t\2\2YZ\7\3\2\2"+
		"Z\27\3\2\2\2[\\\7\3\2\2\\]\7\t\2\2]c\5\32\16\2^_\7\n\2\2_`\7\7\2\2`b\5"+
		"\32\16\2a^\3\2\2\2be\3\2\2\2ca\3\2\2\2cd\3\2\2\2d\31\3\2\2\2ec\3\2\2\2"+
		"fk\7\3\2\2gh\7\3\2\2hi\7\5\2\2ik\7\3\2\2jf\3\2\2\2jg\3\2\2\2k\33\3\2\2"+
		"\2\f\36\'+\60\66;KTcj";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}