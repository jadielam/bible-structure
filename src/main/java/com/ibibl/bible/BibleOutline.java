package com.ibibl.bible;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;


public abstract class BibleOutline {

	private BibleVersion version;
	
	/**
	 * Contains the number of chapters of each of the book
	 */
	private final Map<Book, Integer> bookChapters;
	
	/**
	 * For each chapter of the book, contains the sorted set of verses of the chapter.
	 */
	private final Map<Pair<Book, Integer>, SortedSet<Integer>> chapterVerses;
	
	public BibleOutline(BibleVersion version){
		this.version = version;
		this.bookChapters = new HashMap<>();
		this.chapterVerses = new HashMap<>();
	}
	
	/**
	 * Used to parse the encoded static list of verses of the Bible in order to produce the
	 * Bible outline.
	 * @param encodedList
	 * @param version
	 * @return
	 */
	List<BibleReference> createListOfReferences(String encodedList, BibleVersion version){
		List<BibleReference> references = new ArrayList<BibleReference>();
		String [] stringReferences = encodedList.split("\n");
		for (String stringRef : stringReferences){
			
			int a = stringRef.indexOf(' ');
			int b = stringRef.indexOf(':');
			String bookS = stringRef.substring(0, a);
			String chapterS = stringRef.substring(a+1, b);
			String verseS = stringRef.substring(b+1);
			int bookN = Integer.parseInt(bookS);
			int chapterN = Integer.parseInt(chapterS);
			int verseN = Integer.parseInt(verseS);
			
			Book book = Book.fromPosition(bookN);
			
			BibleReference ref = BibleReference.getUncheckedBibleReference(book, chapterN, verseN, version);
			references.add(ref);
		}
		
		return references;
	}
	
	void createOutline(List<BibleReference> references){
	
		Book currentBook = null;
		Pair<Book, Integer> currentChapterPair = new Pair<>(currentBook, -1);
		SortedSet<Integer> currentVerses = new TreeSet<Integer>();
					
		for (BibleReference ref : references){
			Book book = ref.getBook();
			int chapterInt = ref.getChapter();
			Pair<Book, Integer> chapter = new Pair<>(book, chapterInt);
			int verse = ref.getVerse();
			
			if (!book.equals(currentBook)){
				if (null != currentBook){
					this.bookChapters.put(currentBook, currentChapterPair.getSecond());
				}
				currentBook = book;
			}
			
			if (!currentChapterPair.equals(chapter)){
				this.chapterVerses.put(currentChapterPair, currentVerses);
				currentChapterPair = chapter;
				currentVerses = new TreeSet<>();
				currentVerses.add(verse);
			}
			else {
				currentVerses.add(verse);
			}
		}
		this.bookChapters.put(currentBook, currentChapterPair.getSecond());
		this.chapterVerses.put(currentChapterPair, currentVerses);
			
	}
	/**
	 * Returns the position of this book in the bible
	 * @param book
	 * @return
	 */
	public int getBookPosition(Book book){
		return book.getOrder();
	}
	
	/**
	 * Returns true if such reference exists in this version, otherwise it returns false
	 * @param book
	 * @param chapter
	 * @param verse
	 * @return
	 */
	public boolean validVerse(Book book, int chapter, int verse){
		Pair<Book, Integer> pair = new Pair<Book, Integer>(book, chapter);
		SortedSet<Integer> verses = this.chapterVerses.get(pair);
		if (null == verses) return false;
		return verses.contains(verse);
	}
	/**
	 * Returns the book that corresponds to the given position.
	 * 
	 * @param position
	 * @throws IllegalArgumentException if the position passed as parameter does not exist.
	 * @return
	 */
	public Book getBook(int position) throws IllegalArgumentException{
		return Book.fromPosition(position);
	}
	
	/**
	 * Returns the number of chapters that this book has.
	 * @param book
	 * @return
	 */
	public int getNumberOfChapters(Book book){
		return this.bookChapters.get(book);
	}
	
	/**
	 * Returns the number of verses that this chapter of the Bible has.
	 * Returns -1 if the chapter does not exist
	 */
	public int getNumberOfVerses(Book book, int chapter){
		Pair<Book, Integer> pair = new Pair<>(book, chapter);
		SortedSet<Integer> verses = this.chapterVerses.get(pair);
		if (null == verses) return -1;
		return verses.size();
	}
	
	/**
	 * Return the last verse number that this chapter of the Bible has.
	 * Returns -1 if the chapter does not exist
	 * @param book
	 * @param chapter
	 * @return
	 */
	public int getLastVerse(Book book, int chapter){
		Pair<Book, Integer> pair = new Pair<>(book, chapter);
		SortedSet<Integer> verses = this.chapterVerses.get(pair);
		if (null == verses) return -1;
		return verses.last();
	}
	
	/**
	 * Returns the first verse number that this chapter of the Bible has.
	 * Returns -1 if the chapter does not exist.
	 * @param book
	 * @param chapter
	 * @return
	 */
	public int getFirstVerse(Book book, int chapter){
		Pair<Book, Integer> pair = new Pair<>(book, chapter);
		SortedSet<Integer> verses = this.chapterVerses.get(pair);
		if (null == verses) return -1;
		return verses.first();
	}
	
	/**
	 * Returns an ordered set with the verses for this chapter of the bible.
	 * If the reference is invalid, the returned set is empty.
	 * @param book
	 * @param chapter
	 * @return
	 */
	public SortedSet<Integer> getVerses(Book book, int chapter){
		Pair<Book, Integer> pair = new Pair<>(book, chapter);
		SortedSet<Integer> verses = this.chapterVerses.get(pair);
		if (null == verses) return new TreeSet<Integer>();
		return verses;
	}
	
	/**
	 * Returns the version for this Bible Outline.
	 * @return
	 */
	public BibleVersion getVersion(){
		return this.version;
	}

	
	
}

/**
 * @author jdearmas
 *
 * @since  
 */
class Pair<F, S> {
    private final F first; //first member of pair
    private final S second; //second member of pair
    
    public Pair(F first, S second) {
        this.first = first;
        this.second = second;
    }

    public F getFirst() {
        return first;
    }

    public S getSecond() {
        return second;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (second == null) {
			if (other.second != null)
				return false;
		} else if (!second.equals(other.second))
			return false;
		return true;
	}
    
    
}
