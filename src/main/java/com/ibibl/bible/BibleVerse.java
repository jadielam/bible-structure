package com.ibibl.bible;

public class BibleVerse {

	private final BibleReference reference;
	private final String text;
	
	public BibleVerse(BibleReference reference, String text){
		this.reference = reference;
		this.text = text;
	}

	public BibleReference getReference() {
		return reference;
	}

	public String getText() {
		return text;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((reference == null) ? 0 : reference.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BibleVerse other = (BibleVerse) obj;
		if (reference == null) {
			if (other.reference != null)
				return false;
		} else if (!reference.equals(other.reference))
			return false;
		return true;
	}
	
	
}
