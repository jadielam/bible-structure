package com.ibibl.bible;

public enum Language {
	
	Spanish, English;
}
