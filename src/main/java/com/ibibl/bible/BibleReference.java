package com.ibibl.bible;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;

/**
 * 
 * @author jadiel
 *
 */
public class BibleReference implements Reference {

	private final Book book;
	private final int chapter;
	private final int verse;
	private final BibleVersion version;
		
	private BibleReference(Book book, int chapter, int verse,
			BibleVersion version){
		this.book = book;
		this.chapter = chapter;
		this.verse = verse;
		this.version = version;
	}
	
	public final Book getBook(){
		return this.book;
	}
	
	public final int getChapter(){
		return this.chapter;
	}
	
	public final int getVerse(){
		return this.verse;
	}
	
	public final BibleVersion getVersion(){
		return this.version;
	}
	
	/**
	 * Returns an integer greater than 0 if end > begin
	 * Returns 0 if end = begin
	 * Returns an integer less than 0 of begin > end
	 * @param begin
	 * @param end
	 * @return
	 */
	public static final int compare(BibleReference begin, BibleReference end){
		Book bBook = begin.getBook();
		int bBookPosition = bBook.getOrder();
		int bChapter = begin.getChapter();
		int bVerse = begin.getVerse();
		
		Book eBook = end.getBook();
		int eBookPosition = eBook.getOrder();
		int eChapter = end.getChapter();
		int eVerse = end.getVerse();
		
		if (bBookPosition < eBookPosition){
			return 1;
		}
		else if (bBookPosition > eBookPosition){
			return -1;
		}
		else {
			if (bChapter < eChapter){
				return 1;
			}
			else if (bChapter > eChapter){
				return -1;
			}
			else {
				if (bVerse < eVerse){
					return 1;
				}
				else if (bVerse > eVerse){
					return -1;
				}
				return 0;
			}
		}
		
	}
	
	/**
	 * Creates the Bible Reference with the given parameters.
	 * If the Bible Reference does not exist, it throws an exception
	 * @param book
	 * @param chapter
	 * @param verse
	 * @param version
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static final BibleReference getBibleReference(Book book,
			int chapter, int verse, BibleVersion version) throws IllegalArgumentException{
		
		BibleOutline outline = BibleOutlineFactory.getBibleOutline(version);
		
		int noChapters = outline.getNumberOfChapters(book);
		if (chapter <= 0 || chapter > noChapters) {
			throw new IllegalArgumentException("The chapter number is invalid"); 
		}
		
		SortedSet<Integer> verses = outline.getVerses(book, chapter);
		if (!verses.contains(verse)){
			throw new IllegalArgumentException("The verse number is invalid");
		}
				
		return new BibleReference(book, chapter, verse, version);
	}
	
	/**
	 * Only use inside package when you know what you are doing.
	 * @param book
	 * @param chapter
	 * @param verse
	 * @param version
	 * @return
	 */
	public static final BibleReference getUncheckedBibleReference(Book book, int chapter, int verse,
			BibleVersion version){
		return new BibleReference(book, chapter, verse, version);
	}
	
	@Override
	public String toString(){
		String book = this.book.getName();
		String chapter = Integer.toString(this.chapter);
		String verse = Integer.toString(this.verse);
		
		return book + " " + chapter + ":" + verse;
	}

	/**
	 * version is not included here
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + chapter;
		result = prime * result + verse;
		return result;
	}

	/**
	 * Version is not included here.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BibleReference other = (BibleReference) obj;
		if (book != other.book)
			return false;
		if (chapter != other.chapter)
			return false;
		if (verse != other.verse)
			return false;
		return true;
	}

	@Override
	public List<BibleReference> getBibleReferences() {
		List<BibleReference> toReturn = new ArrayList<BibleReference>();
		toReturn.add(this);
		return toReturn;
	}
	
	
	
	
}
