package com.ibibl.bible;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * BibleReference range with the text included.
 * @author jadiel
 *
 */
public class BibleRange {

	private final BibleReferenceRange range;
	
	private final Map<BibleReference, String> text;
	
	public BibleRange(BibleReferenceRange range, Map<BibleReference, String> text){
		this.range = range;
		List<BibleReference> refs = range.getBibleReferences();
		for (BibleReference ref : refs){
			String a = text.get(ref);
			if (null == a){
				throw new IllegalArgumentException();
			}
		}
		this.text = new HashMap<BibleReference, String>(text);
	}
	
	public List<BibleVerse> getBibleVerses(){
		List<BibleReference> refs = this.range.getBibleReferences();
		List<BibleVerse> verses = new ArrayList<BibleVerse>(refs.size());
		for (BibleReference ref : refs){
			String text = this.text.get(ref);
			BibleVerse verse = new BibleVerse(ref, text);
			verses.add(verse);
		}
		
		return verses;
	}
	
}
