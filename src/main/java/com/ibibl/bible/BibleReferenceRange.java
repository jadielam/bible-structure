package com.ibibl.bible;

import java.util.ArrayList;
import java.util.List;

public class BibleReferenceRange implements Reference {

	private final BibleReference beginning;
	private final BibleReference end;
	
	private BibleReferenceRange(BibleReference beginning,
			BibleReference end){
		
		this.beginning = beginning;
		this.end = end;
	}
	/**
	 * Returns the BibleReference that marks the beginning of
	 * the range, inclusive
	 * @return
	 */
	public final BibleReference beginning(){
		return this.beginning;
	};
	
	/**
	 * Returns the BibleReference that marks the end of the range, 
	 * inclusive
	 * @return
	 */
	public final BibleReference end(){
		return this.end;
	}
	
		
	/**
	 * Returns the ordered list of bible references in the range, 
	 * the beginning bible reference is the first element in the list.
	 * The end bible reference is the last element in the list.
	 * @return
	 */
	public List<BibleReference> getBibleReferences(){
		BibleOutline outline = BibleOutlineFactory.getBibleOutline(this.beginning.getVersion());
		List<BibleReference> refs = new ArrayList<BibleReference>();
		BibleVersion version = this.beginning.getVersion();
				
		Book bBook = beginning.getBook();
		int bBookPosition = bBook.getOrder();
		int bChapter = beginning.getChapter();
		int bVerse = beginning.getVerse();
		
		Book eBook = end.getBook();
		int eBookPosition = eBook.getOrder();
		int eChapter = end.getChapter();
		int eVerse = end.getVerse();

		if (bBook.equals(eBook) && bChapter == eChapter){
			//Add all the verses in the range
			for (int i = bVerse; i <= eVerse; i++){
				try{
					BibleReference toAdd = BibleReference.getBibleReference(bBook, bChapter, i, version);
					refs.add(toAdd);
				}
				catch(IllegalArgumentException e){
					//Do nothing, the verse does not exist.
				}
				
			}
			
		}
		else if (bBook.equals(eBook)){
			//Add all the chapters and verses in the range
			for (int ch_iter = bChapter; ch_iter <= eChapter; ch_iter++){
				int maxVerse = (ch_iter == eChapter) ? eVerse : outline.getLastVerse(bBook, ch_iter);
				int minVerse = (ch_iter == bChapter) ? bVerse : 1;
			
				for (int ver_iter = minVerse; ver_iter <= maxVerse; ver_iter++){
					try{
						BibleReference toAdd = BibleReference.getBibleReference(bBook, ch_iter, ver_iter, version);
						refs.add(toAdd);
					}
					catch(IllegalArgumentException e){
						//Do nothing, the verse does not exist.
					}
					
				}
			}
			
		}
		else {
			//Add all the chapters and verses and books in the range
			for (int book_iter = bBookPosition; book_iter <= eBookPosition; ++book_iter){
				int maxChapter = (book_iter == eBookPosition) ? eChapter : outline.getNumberOfChapters(Book.fromPosition(book_iter));
				int minChapter = (book_iter == bBookPosition) ? bChapter : 1;
				
				for (int ch_iter = minChapter; ch_iter <= maxChapter; ++ch_iter){
					int maxVerse = (ch_iter == eChapter && book_iter == eBookPosition) ? eVerse : outline.getLastVerse(bBook, ch_iter);
					int minVerse = (ch_iter == bChapter && book_iter == bBookPosition) ? bVerse : 1;
				
					for (int ver_iter = minVerse; ver_iter <= maxVerse; ++ver_iter){
						try{
							BibleReference toAdd = BibleReference.getBibleReference(bBook, ch_iter, ver_iter, version);
							refs.add(toAdd);
						}
						catch(IllegalArgumentException e){
							//Do nothing, the verse does not exist.
						}
						
					}
				}
			}
		}
		
		return refs;
	}
	
	/**
	 * Creates a BibleRange object
	 * @param beginning
	 * @param end
	 * @return
	 * @throws IllegalArgumentException whenever the beginning or end are null or the beginning is after the end.
	 */
	public static BibleReferenceRange getBibleReferenceRange(BibleReference beginning,
			BibleReference end) throws IllegalArgumentException {
		if (null == beginning || null == end) throw new IllegalArgumentException();
		if (BibleReference.compare(beginning, end) < 0) {
			throw new IllegalArgumentException("The end of the range is less than the beginning of it.");
		}
		BibleVersion version1 = beginning.getVersion();
		BibleVersion version2 = end.getVersion();
		
		if (!version1.equals(version2)){
			throw new IllegalArgumentException("The references have a different version");
		}
		return new BibleReferenceRange(beginning, end);
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(beginning.toString());
		sb.append(" - ");
		sb.append(end.toString());
		return sb.toString();
	}
}