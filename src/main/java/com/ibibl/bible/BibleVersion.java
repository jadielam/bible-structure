package com.ibibl.bible;

import java.util.HashMap;
import java.util.Map;

public enum BibleVersion {

	KJV(1, "KJV", "King James BibleVersion", Language.English),
	NKJ(2, "NKJ", "New King James BibleVersion", Language.English),
	NIV(3, "NIV", "New International BibleVersion", Language.English),
	ASV(4, "ASV", "American Standard Version", Language.English),
	CEB(5, "CEB", "Common English Bible", Language.English),
	ESV(6, "ESV", "English Standard Version", Language.English),
	NAS(7, "NAS", "New American Standard Bible", Language.English),
	NET(8, "NET", "New English Translation", Language.English),
	NLT(9, "NLT", "New Living Translation", Language.English),
	R60(10, "R60", "Reina Valera 1960", Language.Spanish),
	R95(11, "R95", "Reina Valera 1995", Language.Spanish),
	RVA(12, "RVA", "Reina Valera Antigua", Language.Spanish),
	NVI(13, "NVI", "Nueva Versión Internacional", Language.Spanish);
	
	private final int order;
	private final String abv;
	private final String name;
	private final Language language;
	private static final Map<String, BibleVersion> abvMap;
	
	static{
		abvMap = new HashMap<String, BibleVersion>();
		abvMap.put("KJV", BibleVersion.KJV);
		abvMap.put("NKJ", BibleVersion.NKJ);
		abvMap.put("NIV", BibleVersion.NIV);
		abvMap.put("ASV", BibleVersion.ASV);
		abvMap.put("CEB", BibleVersion.CEB);
		abvMap.put("ESV", BibleVersion.ESV);
		abvMap.put("NAS", BibleVersion.NAS);
		abvMap.put("NET", BibleVersion.NET);
		abvMap.put("NLT", BibleVersion.NLT);
		abvMap.put("R60", BibleVersion.R60);
		abvMap.put("R95", BibleVersion.R95);
		abvMap.put("RVA", BibleVersion.RVA);
		abvMap.put("NVI", BibleVersion.NVI);
		
	}
	
	BibleVersion (int order, String abv, String name, Language language){
		this.order = order;
		this.abv = abv;
		this.name = name;
		this.language = language;
	}

	public int getOrder() {
		return order;
	}

	public String getAbv() {
		return abv;
	}

	public String getName() {
		return name;
	}

	public Language getLanguage() {
		return language;
	}
	
	@Override
	public String toString(){
		return this.abv;
	}
	
	/**
	 * Returns the BibleVersion corresponding to that abbreviation
	 * If no BibleVersion is found, it returns null.
	 * @param avb
	 * @return
	 */
	public static BibleVersion fromAbbreviation(String avb){
		BibleVersion version = BibleVersion.abvMap.get(avb);
		return version;
	}
}

