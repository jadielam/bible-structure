package com.ibibl.bible;

import java.util.List;

public interface Reference {

	/**
	 * 
	 * @return returns all the bible references.
	 */
	public List<BibleReference> getBibleReferences();
}
