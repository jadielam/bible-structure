package com.ibibl.bible.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.ibibl.bible.BibleReference;
import com.ibibl.bible.BibleVersion;
import com.ibibl.bible.Book;

public class BibleWorksFormatParser {

	public static List<BibleReference> getReferences(String filePath, BibleVersion version) throws Exception{
		
		File file = new File(filePath);
		
		List<String> lines = Files.readLines(file, Charsets.UTF_8);
		List<BibleReference> references = new ArrayList<BibleReference>(34000);
		for (String line : lines){
			int a = line.indexOf(' ');
			String bookAbv = null;
			try{
				bookAbv = line.substring(0, a);
			}
			catch(Exception e){
				System.out.println(line);
			}
			Book book = Book.fromAbbreviation(bookAbv);
			if (null == book){
				throw new Exception("The book abbreviation could not be found"
						+ " in the table: "+bookAbv);
			}
			
			int b = line.indexOf(' ', a+1);
			String reference = line.substring(a+1, b);
			int sep = reference.indexOf(':');
			String chapterText = reference.substring(0, sep);
			String verseText = reference.substring(sep+1);
			int chapter = Integer.parseInt(chapterText);
			int verse = Integer.parseInt(verseText);
			
			String text = line.substring(b+1);
			text = text.trim();
			
			if (0 < text.length()){
				
					
					BibleReference ref = BibleReference.getUncheckedBibleReference(book, chapter, verse, version);
					references.add(ref);
				
				
			}
		}
		
		return references;
	}
}
