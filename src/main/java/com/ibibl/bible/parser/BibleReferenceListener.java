// Generated from BibleReference.g4 by ANTLR 4.5
package com.ibibl.bible.parser;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BibleReferenceParser}.
 */
public interface BibleReferenceListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#ref}.
	 * @param ctx the parse tree
	 */
	void enterRef(BibleReferenceParser.RefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#ref}.
	 * @param ctx the parse tree
	 */
	void exitRef(BibleReferenceParser.RefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#book}.
	 * @param ctx the parse tree
	 */
	void enterBook(BibleReferenceParser.BookContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#book}.
	 * @param ctx the parse tree
	 */
	void exitBook(BibleReferenceParser.BookContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#refList}.
	 * @param ctx the parse tree
	 */
	void enterRefList(BibleReferenceParser.RefListContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#refList}.
	 * @param ctx the parse tree
	 */
	void exitRefList(BibleReferenceParser.RefListContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#chapterRef}.
	 * @param ctx the parse tree
	 */
	void enterChapterRef(BibleReferenceParser.ChapterRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#chapterRef}.
	 * @param ctx the parse tree
	 */
	void exitChapterRef(BibleReferenceParser.ChapterRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#simpleRef}.
	 * @param ctx the parse tree
	 */
	void enterSimpleRef(BibleReferenceParser.SimpleRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#simpleRef}.
	 * @param ctx the parse tree
	 */
	void exitSimpleRef(BibleReferenceParser.SimpleRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#chapterOnly}.
	 * @param ctx the parse tree
	 */
	void enterChapterOnly(BibleReferenceParser.ChapterOnlyContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#chapterOnly}.
	 * @param ctx the parse tree
	 */
	void exitChapterOnly(BibleReferenceParser.ChapterOnlyContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#chapterVerse}.
	 * @param ctx the parse tree
	 */
	void enterChapterVerse(BibleReferenceParser.ChapterVerseContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#chapterVerse}.
	 * @param ctx the parse tree
	 */
	void exitChapterVerse(BibleReferenceParser.ChapterVerseContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#chapterVerseRange}.
	 * @param ctx the parse tree
	 */
	void enterChapterVerseRange(BibleReferenceParser.ChapterVerseRangeContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#chapterVerseRange}.
	 * @param ctx the parse tree
	 */
	void exitChapterVerseRange(BibleReferenceParser.ChapterVerseRangeContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#rangeRef}.
	 * @param ctx the parse tree
	 */
	void enterRangeRef(BibleReferenceParser.RangeRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#rangeRef}.
	 * @param ctx the parse tree
	 */
	void exitRangeRef(BibleReferenceParser.RangeRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#chapterRange}.
	 * @param ctx the parse tree
	 */
	void enterChapterRange(BibleReferenceParser.ChapterRangeContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#chapterRange}.
	 * @param ctx the parse tree
	 */
	void exitChapterRange(BibleReferenceParser.ChapterRangeContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#chapterRangeVerse}.
	 * @param ctx the parse tree
	 */
	void enterChapterRangeVerse(BibleReferenceParser.ChapterRangeVerseContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#chapterRangeVerse}.
	 * @param ctx the parse tree
	 */
	void exitChapterRangeVerse(BibleReferenceParser.ChapterRangeVerseContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#multipleVerseRef}.
	 * @param ctx the parse tree
	 */
	void enterMultipleVerseRef(BibleReferenceParser.MultipleVerseRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#multipleVerseRef}.
	 * @param ctx the parse tree
	 */
	void exitMultipleVerseRef(BibleReferenceParser.MultipleVerseRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BibleReferenceParser#verseExp}.
	 * @param ctx the parse tree
	 */
	void enterVerseExp(BibleReferenceParser.VerseExpContext ctx);
	/**
	 * Exit a parse tree produced by {@link BibleReferenceParser#verseExp}.
	 * @param ctx the parse tree
	 */
	void exitVerseExp(BibleReferenceParser.VerseExpContext ctx);
}