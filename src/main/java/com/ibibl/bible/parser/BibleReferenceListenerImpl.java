package com.ibibl.bible.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.ibibl.bible.BibleOutline;
import com.ibibl.bible.BibleReference;
import com.ibibl.bible.BibleReferenceRange;
import com.ibibl.bible.BibleVersion;
import com.ibibl.bible.Book;
import com.ibibl.bible.Reference;
import com.ibibl.bible.parser.BibleReferenceParser.VerseExpContext;

public class BibleReferenceListenerImpl extends BibleReferenceBaseListener {
	
	private final BibleOutline outline;
	
	//Temporary objects collected
	private Book currentBook = null;
	private List<ChapterRef> chapterReferences = new ArrayList<ChapterRef>();
	
	
	//Final object
	private List<Reference> references = new ArrayList<Reference>();
	
	//Overall number of collected references;
	private List<Reference> collectedReferences = new ArrayList<Reference>();
	
	
	public BibleReferenceListenerImpl(BibleOutline outline,
			List<Reference> collectedReferences){
		this.outline = outline;
		this.collectedReferences = collectedReferences;
	}
	
	public List<Reference> getCollectedReferences(){
		return this.collectedReferences;
	}
	
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override 
	public void enterRef(BibleReferenceParser.RefContext ctx) { 
		this.references.clear();
		this.chapterReferences.clear();
		this.currentBook = null;
					
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitRef(BibleReferenceParser.RefContext ctx) { 
		
		//TODO: pass the references collected to a third party object
		for (ChapterRef chapRef : this.chapterReferences){
			this.references.addAll(chapRef.getReferences());
		}
		this.collectedReferences.addAll(this.references);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterBook(BibleReferenceParser.BookContext ctx) { 
		if (null != ctx.GENESIS()){
			this.currentBook = Book.Genesis;
		}
		else if (null != ctx.EXODUS()){
			this.currentBook = Book.Exodus;
		}
		else if (null != ctx.LEVITICUS()){
			this.currentBook = Book.Leviticus;
		}
		else if (null != ctx.NUMBERS()){
			this.currentBook = Book.Numbers;
		}
		else if (null != ctx.DEUTERONOMY()){
			this.currentBook = Book.Deuteronomy;
		}
		else if (null != ctx.JOSHUA()){
			this.currentBook = Book.Joshua;
		}
		else if (null != ctx.JUDGES()){
			this.currentBook = Book.Judges;
		}
		else if (null != ctx.RUTH()){
			this.currentBook = Book.Ruth;
		}
		else if (null != ctx.FIRSTSAMUEL()){
			this.currentBook = Book.FirstSamuel;
		}
		else if (null != ctx.SECONDSAMUEL()){
			this.currentBook = Book.SecondSamuel;
		}
		else if (null != ctx.FIRSTKINGS()){
			this.currentBook = Book.FirstKings;
		}
		else if (null != ctx.SECONDKINGS()){
			this.currentBook = Book.SecondKings;
		}
		else if (null != ctx.FIRSTCHRONICLES()){
			this.currentBook = Book.FirstChronicles;
		}
		else if (null != ctx.SECONDCHRONICLES()){
			this.currentBook = Book.SecondChronicles;
		}
		else if (null != ctx.EZRA()){
			this.currentBook = Book.Ezra;
		}
		else if (null != ctx.NEHEMIAH()){
			this.currentBook = Book.Nehemiah;
		}
		else if (null != ctx.ESTHER()){
			this.currentBook = Book.Esther;
		}
		else if (null != ctx.JOB()){
			this.currentBook = Book.Job;
		}
		else if (null != ctx.PSALMS()){
			this.currentBook = Book.Psalms;
		}
		else if (null != ctx.PROVERBS()){
			this.currentBook = Book.Proverbs;
		}
		else if (null != ctx.ECCLESIASTES()){
			this.currentBook = Book.Ecclesiastes;
		}
		else if (null != ctx.SONGOFSOLOMON()){
			this.currentBook = Book.SongOfSolomon;
		}
		else if (null != ctx.ISAIAH()){
			this.currentBook = Book.Isaiah;
		}
		else if (null != ctx.JEREMIAH()){
			this.currentBook = Book.Jeremiah;
		}
		else if (null != ctx.LAMENTATIONS()){
			this.currentBook = Book.Lamentations;
		}
		else if (null != ctx.EZEKIEL()){
			this.currentBook = Book.Ezekiel;
		}
		else if (null != ctx.DANIEL()){
			this.currentBook = Book.Daniel;
		}
		else if (null != ctx.HOSEA()){
			this.currentBook = Book.Hosea;
		}
		else if (null != ctx.JOEL()){
			this.currentBook = Book.Joel;
		}
		else if (null != ctx.AMOS()){
			this.currentBook = Book.Amos;
		}
		else if (null != ctx.OBADIAH()){
			this.currentBook = Book.Obadiah;
		}
		else if (null != ctx.JONAH()){
			this.currentBook = Book.Jonah;
		}
		else if (null != ctx.MICAH()){
			this.currentBook = Book.Micah;
		}
		else if (null != ctx.NAHUM()){
			this.currentBook = Book.Nahum;
		}
		else if (null != ctx.HABAKKUK()){
			this.currentBook = Book.Habakkuk;
		}
		else if (null != ctx.ZEPHANIAH()){
			this.currentBook = Book.Zephaniah;
		}
		else if (null != ctx.HAGGAI()){
			this.currentBook = Book.Haggai;
		}
		else if (null != ctx.ZECHARIAH()){
			this.currentBook = Book.Zechariah;
		}
		else if (null != ctx.MALACHI()){
			this.currentBook = Book.Malachi;
		}
		else if (null != ctx.MATTHEW()){
			this.currentBook = Book.Matthew;
		}
		else if (null != ctx.MARK()){
			this.currentBook = Book.Mark;
		}
		else if (null != ctx.LUKE()){
			this.currentBook = Book.Luke;
		}
		else if (null != ctx.JOHN()){
			this.currentBook = Book.John;
		}
		else if (null != ctx.ACTS()){
			this.currentBook = Book.Acts;
		}
		else if (null != ctx.ROMANS()){
			this.currentBook = Book.Romans;
		}
		else if (null != ctx.FIRSTCORINTHIANS()){
			this.currentBook = Book.FirstCorinthians;
		}
		else if (null != ctx.SECONDCORINTHIANS()){
			this.currentBook = Book.SecondCorinthians;
		}
		else if (null != ctx.GALATIANS()){
			this.currentBook = Book.Galatians;
		}
		else if (null != ctx.EPHESIANS()){
			this.currentBook = Book.Ephesians;
		}
		else if (null != ctx.PHILIPPIANS()){
			this.currentBook = Book.Philippians;
		}
		else if (null != ctx.COLOSSIANS()){
			this.currentBook = Book.Colossians;
		}
		else if (null != ctx.FIRSTTHESSALONIANS()){
			this.currentBook = Book.FirstThessalonians;
		}
		else if (null != ctx.SECONDTHESSALONIANS()){
			this.currentBook = Book.SecondThessalonians;
		}
		else if (null != ctx.FIRSTTIMOTHY()){
			this.currentBook = Book.FirstTimonthy;
		}
		else if (null != ctx.SECONDTIMOTHY()){
			this.currentBook = Book.SecondTimothy;
		}
		else if (null != ctx.TITUS()){
			this.currentBook = Book.Titus;
		}
		else if (null != ctx.PHILEMON()){
			this.currentBook = Book.Philemon;
		}
		else if (null != ctx.HEBREWS()){
			this.currentBook = Book.Hebrews;
		}
		else if (null != ctx.JAMES()){
			this.currentBook = Book.James;
		}
		else if (null != ctx.FIRSTPETER()){
			this.currentBook = Book.FirstPeter;
		}
		else if (null != ctx.SECONDPETER()){
			this.currentBook = Book.SecondPeter;
		}
		else if (null != ctx.FIRSTJOHN()){
			this.currentBook = Book.FirstJohn;
		}
		else if (null != ctx.SECONDJOHN()){
			this.currentBook = Book.SecondJohn;
		}
		else if (null != ctx.THIRDJOHN()){
			this.currentBook = Book.ThirdJohn;
		}
		else if (null != ctx.JUDE()){
			this.currentBook = Book.Jude;
		}
		else if (null != ctx.REVELATION()){
			this.currentBook = Book.Revelation;
		}
		
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override 
	public void enterChapterOnly(BibleReferenceParser.ChapterOnlyContext ctx) { 
		
		String chapterText = ctx.NUM().getText();
		int chapter = Integer.parseInt(chapterText);
		ChapterOnly chapterOnly = new ChapterOnly(this.currentBook, chapter);
		this.chapterReferences.add(chapterOnly);
	}
	
	@Override 
	public void enterChapterVerse(BibleReferenceParser.ChapterVerseContext ctx) { 
		
		String chapterText = ctx.NUM(0).getText();
		String verseText = ctx.NUM(1).getText();
		
		int chapter = Integer.parseInt(chapterText);
		int verse = Integer.parseInt(verseText);
		
		ChapterVerse chapterVerse = new ChapterVerse(this.currentBook, chapter, verse);
		this.chapterReferences.add(chapterVerse);
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override 
	public void enterChapterVerseRange(BibleReferenceParser.ChapterVerseRangeContext ctx) { 
		String chapterText = ctx.NUM(0).getText();
		String verse1Text = ctx.NUM(1).getText();
		String verse2Text = ctx.NUM(2).getText();
		
		int chapter = Integer.parseInt(chapterText);
		int verse1 = Integer.parseInt(verse1Text);
		int verse2 = Integer.parseInt(verse2Text);
		
		ChapterVerseRange chapterVerseRange = new ChapterVerseRange(this.currentBook, chapter, verse1, verse2);
				
		this.chapterReferences.add(chapterVerseRange);
	}
	
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override 
	public void enterChapterRange(BibleReferenceParser.ChapterRangeContext ctx) { 
		String chapter1Text = ctx.NUM(0).getText();
		String chapter2Text = ctx.NUM(1).getText();
		
		int chapter1 = Integer.parseInt(chapter1Text);
		int chapter2 = Integer.parseInt(chapter2Text);
		
		ChapterRange chapterRange = new ChapterRange(this.currentBook, chapter1, chapter2);
				
		this.chapterReferences.add(chapterRange);
	}
	
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override 
	public void enterChapterRangeVerse(BibleReferenceParser.ChapterRangeVerseContext ctx) { 
		String chapter1Text = ctx.NUM(0).getText();
		
		if (ctx.NUM().size() == 3){
			String chapter2Text = ctx.NUM(1).getText();
			String verse2Text = ctx.NUM(2).getText();
			
			int chapter1 = Integer.parseInt(chapter1Text);
			int chapter2 = Integer.parseInt(chapter2Text);
			int verse2 = Integer.parseInt(verse2Text);
			
			ChapterRangeVerse chapterRangeVerse = new ChapterRangeVerse(this.currentBook, chapter1, chapter2, verse2);
						
			this.chapterReferences.add(chapterRangeVerse);
		}
		else if (ctx.NUM().size() == 4){
			String verse1Text = ctx.NUM(1).getText();
			String chapter2Text = ctx.NUM(2).getText();
			String verse2Text = ctx.NUM(3).getText();
			
			int chapter1 = Integer.parseInt(chapter1Text);
			int verse1 = Integer.parseInt(verse1Text);
			int chapter2 = Integer.parseInt(chapter2Text);
			int verse2 = Integer.parseInt(verse2Text);
			
			ChapterRangeVerse chapterRangeVerse = new ChapterRangeVerse(this.currentBook, chapter1, verse1, chapter2, verse2);
			
			this.chapterReferences.add(chapterRangeVerse);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override 
	public void enterMultipleVerseRef(BibleReferenceParser.MultipleVerseRefContext ctx) { 
		String chapterText = ctx.NUM().getText();
		int chapter = Integer.parseInt(chapterText);
		MultipleVerseRef ref = new MultipleVerseRef(this.currentBook, chapter);
				
		List<VerseExpContext> verseExps = ctx.verseExp();
		for (VerseExpContext vctx : verseExps){
			if (vctx.NUM().size() == 1){
				String verse1Text = vctx.NUM(0).getText();
				int verse1 = Integer.parseInt(verse1Text);
				ref.addVerseExpression(verse1);
			}
			else if (vctx.NUM().size() == 2){
				String verse1Text = vctx.NUM(0).getText();
				String verse2Text = vctx.NUM(1).getText();
				int verse1 = Integer.parseInt(verse1Text);
				int verse2 = Integer.parseInt(verse2Text);
				ref.addVerseExpression(verse1, verse2);
			}
		}
		
		this.chapterReferences.add(ref);
	}
	
	
	private static interface ChapterRef{
		
		public List<Reference> getReferences();
	}
	
	private abstract class SimpleRef implements ChapterRef{
		
	}
	
	/**
	 * Class to represent reference of the form:
	 * Gen 1
	 * @author jadiel
	 *
	 */
	private final class ChapterOnly extends SimpleRef{
		
		private final Book book;
		private final int chapter;
		private List<Reference> references = new ArrayList<Reference>();

		public ChapterOnly(Book book, int chapter){
			this.chapter = chapter;
			this.book = book;
			this.initializeReferences();
		}
		
		private void initializeReferences(){
			BibleOutline outline = BibleReferenceListenerImpl.this.outline;
			
			BibleVersion version = BibleReferenceListenerImpl.this.outline.getVersion();
			int lastVerse = outline.getLastVerse(this.book, this.chapter);
			int firstVerse = outline.getFirstVerse(this.book, this.chapter);
			
			try{
				BibleReference start = BibleReference.getBibleReference(this.book, this.chapter, firstVerse, version);
				BibleReference end = BibleReference.getBibleReference(this.book, this.chapter, lastVerse, version);
				BibleReferenceRange range = BibleReferenceRange.getBibleReferenceRange(start, end);
				this.references.add(range);
			}
			catch(IllegalArgumentException e){
				//Do nothing.
			}
			
		}
		
		@Override
		public List<Reference> getReferences() {
			return Collections.unmodifiableList(this.references);
		}
	}
	
	/**
	 * Class to represent reference of the form:
	 * Gen 1:3
	 * @author jadiel
	 *
	 */
	private final class ChapterVerse extends SimpleRef{
		
		private final Book book;
		private final int chapter;
		private final int verse;
		private final List<Reference> references = new ArrayList<Reference>();
		
		public ChapterVerse(Book book, int chapter, int verse){
			this.book = book;
			this.chapter = chapter;
			this.verse = verse;
			initializeReferences();
			
		}
		
		private void initializeReferences(){
			if (BibleReferenceListenerImpl.this.outline.validVerse(book, chapter, verse)){
				BibleVersion version = BibleReferenceListenerImpl.this.outline.getVersion();
				try{
					BibleReference ref = BibleReference.getBibleReference(this.book, this.chapter, this.verse, version);
					this.references.add(ref);
				}
				catch(IllegalArgumentException e){
					
				}
				
			}
			 
		}
		@Override
		public List<Reference> getReferences() {
			return Collections.unmodifiableList(this.references);
		}
	}
	
	/**
	 * Class to represent reference of the form:
	 * Gen 1:1-3
	 * @author jadiel
	 *
	 */
	private final class ChapterVerseRange extends SimpleRef{
		
		private final Book book;
		private final int chapter;
		private final int verse1;
		private final int verse2;
		private final List<Reference> references = new ArrayList<Reference>();
		
		public ChapterVerseRange(Book book, int chapter, int verse1, int verse2){
			this.book = book;
			this.chapter = chapter;
			this.verse1 = verse1;
			this.verse2 = verse2;
			initializeReferences();
		}
		
		private void initializeReferences(){
			BibleOutline outline = BibleReferenceListenerImpl.this.outline;
			BibleVersion version = outline.getVersion();
			try{
				BibleReference begin = BibleReference.getBibleReference(this.book, this.chapter, this.verse1, version);
				BibleReference end = BibleReference.getBibleReference(this.book, this.chapter, this.verse2, version);
				BibleReferenceRange range = BibleReferenceRange.getBibleReferenceRange(begin, end);
				this.references.add(range);
			}
			catch(IllegalArgumentException e){
				//do nothing.
			}
			
		}
		
		@Override
		public List<Reference> getReferences() {
			return Collections.unmodifiableList(this.references);
		}
	}
	
	private static abstract class RangeRef implements ChapterRef{
		
	}
	
	/**
	 * Class to represent a reference of the form
	 * Gen 1-3
	 * @author jadiel
	 *
	 */
	private final class ChapterRange extends RangeRef{
		private final Book book;
		private final int chapter1;
		private final int chapter2;
		private final List<Reference> references = new ArrayList<Reference>();
		
		public ChapterRange(Book book, int chapter1, int chapter2){
			this.book = book;
			this.chapter1 = chapter1;
			this.chapter2 = chapter2;
			initializeReferences();
		}
		
		private void initializeReferences(){
			BibleOutline outline = BibleReferenceListenerImpl.this.outline;
			BibleVersion version = outline.getVersion();
			int lastVerse = outline.getLastVerse(this.book, this.chapter2);
			int firstVerse = outline.getFirstVerse(this.book, this.chapter1);
			try{
				BibleReference begin = BibleReference.getBibleReference(this.book, this.chapter1, firstVerse, version);
				BibleReference end = BibleReference.getBibleReference(this.book, this.chapter2, lastVerse, version);
				BibleReferenceRange range = BibleReferenceRange.getBibleReferenceRange(begin, end);
				this.references.add(range);
			}
			catch(IllegalArgumentException e){
				//Do nothing.
			}
		
		}
		
		@Override
		public List<Reference> getReferences() {
			return Collections.unmodifiableList(this.references);
		}
	}
	
	/**
	 * Class to represent a reference of one of the two following forms:
	 * Gen 1:1-3:4
	 * Gen 1-3:4
	 * @author jadiel
	 *
	 */
	private final class ChapterRangeVerse extends RangeRef {
		
		private final Book book;
		private final int chapter1;
		private final int verse1;
		private final int chapter2;
		private final int verse2;
		private List<BibleReferenceRange> references = new ArrayList<BibleReferenceRange>();
		
		public ChapterRangeVerse(Book book, int chapter1, int chapter2, int verse2){
			this.book = book;
			this.chapter1 = chapter1;
			this.chapter2 = chapter2;
			this.verse2 = verse2;
			this.verse1 = -1;
			initializeReferences();
		}
		
		public ChapterRangeVerse(Book book, int chapter1, int verse1, int chapter2, int verse2){
			this.book = book;
			this.chapter1 = chapter1;
			this.verse1 = verse1;
			this.chapter2 = chapter2;
			this.verse2 = verse2;
			initializeReferences();
		}

		private void initializeReferences(){
			
			BibleOutline outline = BibleReferenceListenerImpl.this.outline;
			BibleVersion version = outline.getVersion();
			
			
			
			if (-1 == this.verse1){
				int firstVerse = outline.getFirstVerse(this.book,  this.chapter1);
				
				try{
					BibleReference beginning = BibleReference.getBibleReference(this.book, this.chapter1, firstVerse, version);
					BibleReference end = BibleReference.getBibleReference(this.book, this.chapter2, this.verse2, version);
					BibleReferenceRange range = BibleReferenceRange.getBibleReferenceRange(beginning, end);
					this.references.add(range);
				}
				catch(IllegalArgumentException e){
					//do nothing.
				}
			}
				
			else{
				try{
					BibleReference beginning = BibleReference.getBibleReference(this.book, this.chapter1, this.verse1, version);
					BibleReference end = BibleReference.getBibleReference(this.book, this.chapter2, this.verse2, version);
					BibleReferenceRange range = BibleReferenceRange.getBibleReferenceRange(beginning, end);
					this.references.add(range);
				}
				catch(IllegalArgumentException e){
					//Do nothing
				}			
			}
		}
		
		@Override
		public List<Reference> getReferences() {
			return Collections.unmodifiableList(this.references);
		}
	}
	
	/**
	 * Class to represent Bible References of the form
	 * Gen 1:1, 3, 5, 20-22
	 * @author jadiel
	 *
	 */
	private final class MultipleVerseRef implements ChapterRef{
		private final Book book;
		private final int chapter1;
		private final List<VerseExpr> verseExpressions = new ArrayList<VerseExpr>();
				
		public MultipleVerseRef(Book book, int chapter1){
			this.book = book;
			this.chapter1 = chapter1;
		}

		/**
		 * For this class I have to defer the calculation of the references
		 * until the call time of this function, because this class can change with time,
		 * since I can add verse expressions to it after construction time.
		 * 
		 */
		@Override
		public List<Reference> getReferences() {
			List<Reference> toReturn = new ArrayList<Reference>();
			BibleOutline outline = BibleReferenceListenerImpl.this.outline;
			BibleVersion version = outline.getVersion();
						
			for (VerseExpr expr : this.verseExpressions){
				int verse1 = expr.verse1();
				int verse2 = expr.verse2();
				
				if (-1 == verse2){
					try{
						BibleReference reference = BibleReference.getBibleReference(this.book, this.chapter1, verse1, version);
						toReturn.add(reference);
					}
					catch(IllegalArgumentException e){
						
					}
					
				}
				else{
					try{
						BibleReference beginning = BibleReference.getBibleReference(this.book, this.chapter1, verse1, version);
						BibleReference end = BibleReference.getBibleReference(this.book, this.chapter1, verse2, version);
						BibleReferenceRange range = BibleReferenceRange.getBibleReferenceRange(beginning, end);
						toReturn.add(range);
					}
					catch(IllegalArgumentException e){
						//Do nothing.
					}
					
				}
			}
			
			return toReturn;
		}

		public void addVerseExpression(int verse){
			VerseExpr expr = new VerseExpr(verse);
			this.verseExpressions.add(expr);
		}
		
		public void addVerseExpression(int verse1, int verse2){
			VerseExpr expr = new VerseExpr(verse1, verse2);
			this.verseExpressions.add(expr);
		}
		
		private final class VerseExpr{
			private final int verse1;
			private final int verse2;
			
			public VerseExpr(int verse1){
				this.verse1 = verse1;
				this.verse2 = -1;
			}
			
			public VerseExpr(int verse1, int verse2){
				this.verse1 = verse1;
				this.verse2 = verse2;
			}
			
			public int verse1(){ 
				return verse1;
			}
			
			public int verse2(){ 
				return verse2;
			}
		}
	}
}
