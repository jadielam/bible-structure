package com.ibibl.bible.parser;

import java.util.regex.Pattern;

/**
 * Regular expression for the simple Bible references.
 * @author jadiel
 *
 */
public class SimpleBibleReferenceReg {

	private static final String BOOK_REG;
	private static final String NUM;
	private static final String SPACE;
	private static final String SIMPLE_REFERENCE_REG;
	public static final Pattern REF_PATTERN;
		
	static {
		SPACE = "[\t ]";
		NUM = "[1-9][0-9]{0,2}";
		BOOK_REG = "[a-zA-Z]+";
		SIMPLE_REFERENCE_REG = BOOK_REG + "[\\.]?" + SPACE + NUM + "[:\\. ]" + NUM;
		REF_PATTERN = Pattern.compile(SIMPLE_REFERENCE_REG);
	}
	
}
	
