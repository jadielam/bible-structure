package com.ibibl.bible.parser;

import java.util.regex.Pattern;


public class BibleReferenceReg {

	private static final String BOOK_REG;
	private static final String REFLIST_REG;
	public static final String REF_REG;
	private static final String CHAPTERREF_REG;
	private static final String SIMPLEREF_REG;
	private static final String RANGEREF_REG;
	private static final String CHAPTERRANGE_REG;
	private static final String CHAPTERRANGEVERSE_REG;
	private static final String MULTIPLEVERSEREF_REG;
	private static final String VERSEEXP_REG;
	private static final String NUM;
	private static final String SPACE;
	public static final Pattern REF_PATTERN;
		
	static {
		SPACE = "[\t \r\n]";
		NUM = "[1-9][0-9]{0,2}";
		VERSEEXP_REG = "(" + NUM + "(" + "(" +SPACE+")*" + "-" + "(" +SPACE+"+)*"+ NUM+")?)";
		MULTIPLEVERSEREF_REG = NUM + "[:\\. ]" + VERSEEXP_REG + "(" + "(" +SPACE+")*" + ",("+SPACE+")*"+VERSEEXP_REG+")*";
		CHAPTERRANGEVERSE_REG = NUM + "([:\\. ]"+NUM+")?" + "(" +SPACE+")*" + "-"+ "(" +SPACE+")*"+ NUM + "(" +SPACE+")*"+ ":"+"(" +SPACE+")*"+ NUM;
		CHAPTERRANGE_REG = NUM + "(" +SPACE+")*"+"-" + "(" +SPACE+")*"+ NUM;
		RANGEREF_REG = "("+CHAPTERRANGEVERSE_REG + "|" + CHAPTERRANGE_REG+")";
		SIMPLEREF_REG = NUM + "(" + "(" +SPACE+")*"+ ":" + "(" +SPACE+")*"+ NUM + "(" + "(" +SPACE+")*"+ "-" + "(" +SPACE+")*"+ NUM + ")?" + ")?";
		CHAPTERREF_REG = "(" + MULTIPLEVERSEREF_REG + "|" + RANGEREF_REG + "|"+ SIMPLEREF_REG + ")";
		REFLIST_REG = "(" + CHAPTERREF_REG +"("+"(" +SPACE+")*"+";" + "(" + SPACE +"+)?" + CHAPTERREF_REG+")*)";
		BOOK_REG = "[a-zA-Z]+";
		REF_REG = BOOK_REG + "[\\.]?" + "(" +SPACE+"+)?" + REFLIST_REG;
		REF_PATTERN = Pattern.compile(REF_REG);
	}
	
	

}
