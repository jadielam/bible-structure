package com.ibibl.bible;

public class BibleOutlineFactory {

	/**
	 * Returns the BibleOutline corresponding to that version of the Bible.
	 * By default returns the NIVBibleOutline.
	 * @param version
	 * @return
	 */
	public static BibleOutline getBibleOutline(BibleVersion version){
		if (version.equals(BibleVersion.NIV)){
			return NIVBibleOutline.getInstance();
		}
		else if (version.equals(BibleVersion.ASV)){
			return ASVBibleOutline.getInstance();
		}
		else if (version.equals(BibleVersion.CEB)){
			return CEBBibleOutline.getInstance();
		}
		else if (version.equals(BibleVersion.ESV)){
			return ESVBibleOutline.getInstance();
		}
		else if (version.equals(BibleVersion.KJV)){
			return KJVBibleOutline.getInstance();
		}
		else if (version.equals(BibleVersion.NET)){
			return NETBibleOutline.getInstance();
		}
		else if (version.equals(BibleVersion.NKJ)){
			return NKJBibleOutline.getInstance();
		}
		else if (version.equals(BibleVersion.NLT)){
			return NLTBibleOutline.getInstance();
		}
		else if (version.equals(BibleVersion.R60)){
			return R60BibleOutline.getInstance();
		}
		else if (version.equals(BibleVersion.R95)){
			return R95BibleOutline.getInstance();
		}
		else if (version.equals(BibleVersion.RVA)){
			return RVABibleOutline.getInstance();
		}
		else if (version.equals(BibleVersion.NVI)){
			return NVIBibleOutline.getInstance();
		}
		else{
			return NETBibleOutline.getInstance();
		}
	}
}
