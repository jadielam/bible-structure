package com.ibibl.bible;

import java.util.Map;
import java.util.HashMap;
/**
 * The books of the Bible.
 * Does not include apocripha here.
 * @author jadiel
 *
 */
public enum Book {

	Genesis(1, "Genesis", "Gen"), 
	Exodus(2, "Exodus", "Exo"), 
	Leviticus(3, "Leviticus", "Lev"),
	Numbers(4, "Numbers", "Num"),
	Deuteronomy(5, "Deuteronomy", "Deu"),
	Joshua(6, "Joshua", "Jos"),
	Judges(7, "Judges", "Jdg"),
	Ruth(8, "Ruth", "Rut"),
	FirstSamuel(9, "1 Samuel", "1Sa"),
	SecondSamuel(10, "2 Samuel", "2Sa"),
	FirstKings(11, "1 Kings", "1Ki"),
	SecondKings(12, "2 Kings", "2Ki"),
	FirstChronicles(13, "1 Chronicles", "1Ch"),
	SecondChronicles(14, "2 Chronicles", "2Ch"),
	Ezra(15, "Ezra", "Ezr"),
	Nehemiah(16, "Nehemiah", "Neh"),
	Esther(17, "Esther", "Est"),
	Job(18, "Job", "Job"),
	Psalms(19, "Psalms", "Psa"),
	Proverbs(20, "Proverbs", "Pro"),
	Ecclesiastes(21, "Ecclesiastes", "Ecc"),
	SongOfSolomon(22, "Song of Solomon", "Sol"),
	Isaiah(23, "Isaiah", "Isa"),
	Jeremiah(24, "Jeremiah", "Jer"),
	Lamentations(25, "Lamentations", "Lam"),
	Ezekiel(26, "Ezekiel", "Eze"),
	Daniel(27, "Daniel", "Dan"),
	Hosea(28, "Hosea", "Hos"),
	Joel(29, "Joel", "Joe"),
	Amos(30, "Amos", "Amo"),
	Obadiah(31, "Obadiah", "Oba"),
	Jonah(32, "Jonah", "Jon"),
	Micah(33, "Micah", "Mic"),
	Nahum(34, "Nahum", "Nah"),
	Habakkuk(35, "Habakkuk", "Hab"),
	Zephaniah(36, "Zephaniah", "Zep"),
	Haggai(37, "Haggai", "Hag"),
	Zechariah(38, "Zechariah", "Zec"),
	Malachi(39, "Malachi", "Mal"),
	Matthew(40, "Matthew", "Mat"),
	Mark(41, "Mark", "Mar"),
	Luke(42, "Luke", "Luk"),
	John(43, "John", "Joh"),
	Acts(44, "Acts", "Act"),
	Romans(45, "Romans", "Rom"),
	FirstCorinthians(46, "1 Corinthians", "1Co"),
	SecondCorinthians(47, "2 Corinthians", "2Co"),
	Galatians(48, "Galatians", "Gal"),
	Ephesians(49, "Ephesians", "Eph"),
	Philippians(50, "Philippians", "Phi"),
	Colossians(51, "Colossians", "Col"),
	FirstThessalonians(52, "1 Thessalonians", "1Th"),
	SecondThessalonians(53, "2 Thessalonians", "2Th"),
	FirstTimonthy(54, "1 Timothy", "1Ti"),
	SecondTimothy(55, "2 Timothy", "2Ti"),
	Titus(56, "Titus", "Tit"),
	Philemon(57, "Philemon", "Phm"),
	Hebrews(58, "Hebrews", "Heb"),
	James(59, "James", "Jam"),
	FirstPeter(60, "1 Peter", "1Pe"),
	SecondPeter(61, "2 Peter", "2Pe"),
	FirstJohn(62, "1 John", "1Jo"),
	SecondJohn(63, "2 John", "2Jo"),
	ThirdJohn(64, "3 John", "3Jo"),
	Jude(65, "Jude", "Jud"),
	Revelation(66, "Revelation", "Rev");
	
	private final int order;
	private final String name;
	private final String abv;
	private static final Map<String, Book> ab;
	
	static {
		ab = new HashMap<String, Book>();
		ab.put("Gen", Book.Genesis);
		ab.put("Exo", Book.Exodus);
		ab.put("Lev", Book.Leviticus);
		ab.put("Num", Book.Numbers);
		ab.put("Deu", Book.Deuteronomy);
		ab.put("Jos", Book.Joshua);
		ab.put("Jdg", Book.Judges);
		ab.put("Rut", Book.Ruth);
		ab.put("1Sa", Book.FirstSamuel);
		ab.put("2Sa", Book.SecondSamuel);
		ab.put("1Ki", Book.FirstKings);
		ab.put("2Ki", Book.SecondKings);
		ab.put("1Ch", Book.FirstChronicles);
		ab.put("2Ch", Book.SecondChronicles);
		ab.put("Ezr", Book.Ezra);
		ab.put("Neh", Book.Nehemiah);
		ab.put("Est", Book.Esther);
		ab.put("Job", Book.Job);
		ab.put("Psa", Book.Psalms);
		ab.put("Pro", Book.Proverbs);
		ab.put("Ecc", Book.Ecclesiastes);
		ab.put("Sol", Book.SongOfSolomon);
		ab.put("Isa", Book.Isaiah);
		ab.put("Jer", Book.Jeremiah);
		ab.put("Lam", Book.Lamentations);
		ab.put("Eze", Book.Ezekiel);
		ab.put("Dan", Book.Daniel);
		ab.put("Hos", Book.Hosea);
		ab.put("Joe", Book.Joel);
		ab.put("Amo", Book.Amos);
		ab.put("Oba", Book.Obadiah);
		ab.put("Jon", Book.Jonah);
		ab.put("Mic", Book.Micah);
		ab.put("Nah", Book.Nahum);
		ab.put("Hab", Book.Habakkuk);
		ab.put("Zep", Book.Zephaniah);
		ab.put("Hag", Book.Haggai);
		ab.put("Zec", Book.Zechariah);
		ab.put("Mal",  Book.Malachi);
		ab.put("Mat", Book.Matthew);
		ab.put("Mar", Book.Mark);
		ab.put("Luk", Book.Luke);
		ab.put("Joh", Book.John);
		ab.put("Act", Book.Acts);
		ab.put("Rom", Book.Romans);
		ab.put("1Co", Book.FirstCorinthians);
		ab.put("2Co", Book.SecondCorinthians);
		ab.put("Gal", Book.Galatians);
		ab.put("Eph", Book.Ephesians);
		ab.put("Phi", Book.Philippians);
		ab.put("Col", Book.Colossians);
		ab.put("1Th", Book.FirstThessalonians);
		ab.put("2Th", Book.SecondThessalonians);
		ab.put("1Ti", Book.FirstTimonthy);
		ab.put("2Ti", Book.SecondTimothy);
		ab.put("Tit", Book.Titus);
		ab.put("Phm", Book.Philemon);
		ab.put("Heb", Book.Hebrews);
		ab.put("Jam", Book.James);
		ab.put("1Pe", Book.FirstPeter);
		ab.put("2Pe", Book.SecondPeter);
		ab.put("1Jo", Book.FirstJohn);
		ab.put("2Jo", Book.SecondJohn);
		ab.put("3Jo", Book.ThirdJohn);
		ab.put("Jud", Book.Jude);
		ab.put("Rev", Book.Revelation);
	
	}
	
	Book (int order, String name, String abv){
		this.order = order;
		this.name = name;
		this.abv = abv;
	}
	
	public int getOrder(){
		return this.order;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getAbv(){
		return this.abv;
	}
	
	/**
	 * Returns the Book given the position of the book in the Bible.
	 * Note that valid positions are integers between 1 and 66
	 * @param value
	 * @return
	 */
	public static Book fromPosition(int value){
		try{
			return Book.values()[value-1];
		} catch (ArrayIndexOutOfBoundsException e){
			throw new IllegalArgumentException("Book order outside of range: "+value);
		}
	}
	
	/**
	 * Returns the book corresponding to that abbreviation.
	 * If no book is found corresponding to that abbreviation, it 
	 * returns null
	 * @param abbv
	 * @return
	 */
	public static Book fromAbbreviation(String abbv){
		Book book = Book.ab.get(abbv);
		return book;
	}
	
	@Override
	public String toString(){
		return name;
	}
	
}