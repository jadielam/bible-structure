grammar BibleReference; 

ref: book DOT? SPACE* refList;

book: AMOS | FIRSTCHRONICLES | SECONDCHRONICLES | DANIEL | DEUTERONOMY | ECCLESIASTES | ESTHER | EXODUS |
	  EZEKIEL | EZRA | GENESIS | HABAKKUK | HAGGAI | HOSEA | ISAIAH | JEREMIAH | JOB | JOEL | JONAH |
	  JOSHUA | JUDGES | FIRSTKINGS | SECONDKINGS | LAMENTATIONS | LEVITICUS | MALACHI | MICAH |
	  NAHUM | NEHEMIAH | NUMBERS | OBADIAH | PROVERBS | PSALMS | RUTH | FIRSTSAMUEL | SECONDSAMUEL | SONGOFSOLOMON |
	  ZECHARIAH | ZEPHANIAH | 
	  ACTS | REVELATION | COLOSSIANS | FIRSTCORINTHIANS | SECONDCORINTHIANS | EPHESIANS | GALATIANS |
	  HEBREWS | JAMES | JOHN | FIRSTJOHN | SECONDJOHN | THIRDJOHN | JUDE | LUKE | MARK | MATTHEW | FIRSTPETER | SECONDPETER | PHILEMON |
	  PHILIPPIANS | ROMANS | FIRSTTHESSALONIANS | SECONDTHESSALONIANS | FIRSTTIMOTHY | SECONDTIMOTHY | TITUS ;

refList: chapterRef | (chapterRef (SPACE* PUNTOYCOMA SPACE* chapterRef)*) ;

chapterRef: (simpleRef | rangeRef | multipleVerseRef) ;

simpleRef: ( chapterOnly | chapterVerse | chapterVerseRange) ;
chapterOnly: NUM ;
chapterVerse: NUM (':'|DOT|SPACE+) NUM;
chapterVerseRange: NUM (':'|DOT) NUM '-' NUM ;

rangeRef : (chapterRange | chapterRangeVerse) ;
chapterRange : NUM SPACE* '-' SPACE* NUM ;
chapterRangeVerse : NUM (SPACE* (':'|DOT) SPACE* NUM)? SPACE* '-' SPACE* NUM (':'|DOT) SPACE* NUM ;

multipleVerseRef : NUM SPACE* (':'|DOT) SPACE* verseExp (SPACE* ',' SPACE* verseExp)* ;
verseExp : (NUM | (NUM SPACE* '-' SPACE* NUM)) ;

//TOKENS

//Chapter and verse numbers
NUM: [1-9][0-9]* ;  
DOT: '.' ;
GUION: '-' ;
DOSPUNTOS: ':' ;
SPACE : ('\t' | ' ' | '\r' | '\n'); //{skip();};
PUNTOYCOMA : ';' ;
SEPARATOR : DOSPUNTOS | DOT | ' ' ;
COMA : ',' ;


//Old Testament books in alphabetical order
AMOS: 'Amos' | 'Am' | 'amos' | 'am' ;
FIRSTCHRONICLES : '1 Chronicles' | '1 Chron' | '1 Chr' | '1Chron' | '1Chr' | '1 chronicles' | '1 chron' | '1 crh' | '1chron' | '1chr' ;
SECONDCHRONICLES : '2 Chronicles' | '2 Chron' | '2 Chr' | '2Chron' | '2Chr' | '2 chronicles' | '2 chron' | '2 crh' | '2chron' | '2chr' ;
DANIEL : 'Daniel' | 'Dan' | 'Dn' | 'daniel' | 'dan' | 'dn' ;
DEUTERONOMY : 'Deuteronomy' | 'Deut' | 'Dt' | 'deuteronomy' | 'Deutoronomy' | 'deutoronomy' | 'deut' | 'dt' ;
ECCLESIASTES : 'Ecclesiastes' | 'Eccles' | 'Eccl' | 'ecclesiastes' | 'eclesiastes' | 'Eclesiastes' | 'eccles' | 'eccl' ;
ESTHER : 'Esther' | 'Est' | 'Ester' | 'esther' | 'est' ;
EXODUS: 'Exodus' | 'Exod' | 'Exo' | 'Ex' | 'exodus' | 'exod' | 'exo' | 'ex' ;
EZEKIEL : 'Ezekiel' | 'Ezek' | 'Ez' | 'ezekiel' | 'ezek' | 'exek' | 'Exekiel' | 'ez' ;
EZRA: 'Ezra' | 'Ezr' | 'ezra' | 'ezr' ;
GENESIS: 'Genesis' | 'Gen' | 'Gn' | 'genesis' | 'gen' | 'gn' ;
HABAKKUK: 'Habakkuk' | 'Hab' | 'Hb' | 'habakkuk' | 'hab' | 'hb' ;
HAGGAI: 'Haggai' | 'Hag' | 'Hg' | 'haggai' | 'hag' | 'hg' ;
HOSEA: 'Hosea' | 'Hos' | 'hosea' | 'hos' ;
ISAIAH: 'Isaiah' | 'Isa' | 'Is' | 'isaiah' | 'isa' | 'is' ;
JEREMIAH: 'Jeremiah' | 'Jer' | 'jeremiah' | 'jer' ;
JOB: 'Job' | 'Jb' | 'job' | 'jb';
JOEL: 'Joel' | 'Jl' | 'joel' | 'jl' ;
JONAH: 'Jonah' | 'Jon' | 'jonah' | 'jon' ;
JOSHUA: 'Joshua' | 'Josh' | 'Jo' | 'joshua' | 'josh' | 'jo' ;
JUDGES: 'Judges' | 'Judg' | 'Jgs' | 'judges' | 'judg' | 'jgs' ;
FIRSTKINGS: '1 Kings' | '1 Kgs' | '1Kings' | '1Kgs' | '1 kings' | '1 kgs' | '1kings' | '1kgs' ;
SECONDKINGS: '2 Kings' | '2 Kgs' | '2Kings' | '2Kgs' | '2 kings' | '2 kgs' | '2kgs' ;
LAMENTATIONS: 'Lamentations' | 'Lam' | 'Lm' | 'lamentations' | 'lamentation' | 'Lamentation' | 'lam' | 'lm';
LEVITICUS: 'Leviticus' | 'Lev' | 'Lv' | 'leviticus' | 'lev' | 'lv' ;
MALACHI: 'Malachi' | 'Mal' | 'malachi' | 'mal' ;
MICAH: 'Micah' | 'Mic' | 'Mi' | 'micah' | 'mic' | 'mi';
NAHUM: 'Nahum' | 'Nah' | 'Na' | 'nahum' | 'nah' | 'na' ;
NEHEMIAH: 'Nehemiah' | 'Neh' | 'nehemiah' | 'neh' ;
NUMBERS: 'Numbers' | 'Num' | 'Nm' | 'numbers' | 'num' | 'nm' ;
OBADIAH: 'Obadiah' | 'Obad' | 'Ob' | 'obadiah' | 'obad' | 'ob';
PROVERBS: 'Proverbs' | 'Prov' | 'Prv' | 'proverbs' | 'prov' | 'prv' ;
PSALMS: 'Psamls' | 'Psalm' | 'Ps' | 'Pss' | 'Psalms' | 'psamls' | 'psalms' | 'psalm' | 'ps' | 'pss' ;
RUTH: 'Ruth' | 'Ru' | 'ruth' | 'ru' ;
FIRSTSAMUEL: '1 Samuel' | '1 Sam' | '1Samuel' | '1Sam' | '1 samuel' | '1 sam' | '1samuel' | '1sam' ;
SECONDSAMUEL: '2 Samuel' | '2 Sam' | '2Samuel' | '2Sam' | '2 samuel' | '2 sam' | '2samuel' ;
SONGOFSOLOMON: 'Song of Solomon' | 'Song of Sol' | 'Sg' | 'Song' | 'Song of Songs' | 'song of solomon' | 'song of sol' | 'sg' | 'song' | 'song of songs';
ZECHARIAH: 'Zechariah' | 'Zech' | 'Zec' | 'zechariah' | 'zech' | 'zec' ;
ZEPHANIAH: 'Zephaniah' | 'Zeph' | 'Zep' | 'zephania' | 'zeph' | 'zep' ;

//New Testament books in alphabetical order
ACTS: 'Acts of the Apostles' | 'Acts' | 'acts of the apostles' | 'acts' | 'Act' | 'act' ;
REVELATION: 'Revelation' | 'Apocalypse' | 'Ap' | 'Apoc' | 'Rev' | 'Rv' | 'revelation' | 'rev' | 'rv' | 'ap' | 'apoc';
COLOSSIANS: 'Colossians' | 'Col' | 'colossians' | 'col';
FIRSTCORINTHIANS: '1 Corinthians' | '1 Cor' | '1Corinthians' | '1Cor' | '1 corinthians' | '1 cor' | '1corinthians' | '1cor';
SECONDCORINTHIANS: '2 Corinthians' | '2 Cor' | '2Corinthians' | '2Cor' | '2 corinthians' | '2 cor' | '2corinthians' | '2cor';
EPHESIANS: 'Ephesians' | 'Eph' | 'ephesians' | 'eph' ;
GALATIANS: 'Galatians' | 'Gal' | 'galatians' | 'gal';
HEBREWS: 'Hebrews' | 'Heb' | 'hebrews' | 'heb';
JAMES: 'James' | 'Jam' | 'Jas' | 'james' | 'jam' | 'jas' ;
JOHN: 'John' | 'Jn' | 'john' | 'jn' ;
FIRSTJOHN: '1 John' | '1 Jn' | '1John' | '1Jn' | '1 john' | '1 jn' | '1john' | '1jn' ;
SECONDJOHN: '2 John' | '2 Jn' | '2John' | '2Jn' | '2 john' | '2 jn' | '2john' | '2jn';
THIRDJOHN: '3 John' | '3 Jn' | '3John' | '3Jn' | '3 john' | '3 jn' | '3john' | '3jn' ;
JUDE: 'Jude' | 'Jd' | 'jude' | 'jd' ;
LUKE: 'Luke' | 'Luk' | 'luke' | 'luk';
MARK: 'Mark' | 'Mk' | 'mark' | 'mk' ;
MATTHEW: 'Matthew' | 'Matt' | 'Mt' | 'matthew' | 'matt' | 'mt' ;
FIRSTPETER: '1 Peter' | '1 Pet' | '1 Pt' | '1Peter' | '1Pet' | '1Pt' | '1 peter' | '1 pet' | '1 pt' | '1peter' | '1pet' | '1pt' ;
SECONDPETER: '2 Peter' | '2 Pet' | '2 Pt' | '2Peter' | '2Pet' | '2Pt' |  '2 peter' | '2 pet' | '2 pt' | '2peter' | '2pet' | '2pt' ;
PHILEMON: 'Philemon' | 'Philem' | 'Phlm' | 'philemon' | 'philem' | 'phlm' ;
PHILIPPIANS: 'Philippians' | 'Phil' | 'philippians' | 'phil' ;
ROMANS: 'Romans' | 'Rom' | 'romans' | 'rom' ;
FIRSTTHESSALONIANS: '1 Thessalonians' | '1 Thess' | '1 Thes' | '1Thessalonians' | '1Thess' | '1Thes' | '1 thessalonians' | '1 thess' | '1 thes' | '1thessalonians' | '1thess' | '1thes' ;
SECONDTHESSALONIANS: '2 Thessalonians' | '2 Thess' | '2 Thes' | '2Thessalonians' | '2Thess' | '2Thes' | '2 thessalonians' | '2 thess' | '2 thes' | '2thessalonians' | '2thess' | '2thes' ;
FIRSTTIMOTHY: '1 Timothy' | '1 Tim' | '1 Tm' | '1Timothy' | '1Tim' | '1Tm' | '1 timothy' | '1 tim' | '1 tm' | '1timothy' | '1tm' | '1tim' ;
SECONDTIMOTHY: '2 Timothy' | '2 Tim' | '2 Tm' | '2Timothy' | '2Tim' | '2Tm'  | '2 timothy' | '2 tim' | '2 tm' | '2timothy' | '2tm' | '2tim' ;
TITUS: 'Titus' | 'Ti' | 'titus' | 'ti' ;