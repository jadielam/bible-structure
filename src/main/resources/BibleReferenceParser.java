// Generated from BibleReference.g4 by ANTLR 4.5
package com.ibibl.bible.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BibleReferenceParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NUM=1, DOT=2, GUION=3, DOSPUNTOS=4, SPACE=5, PUNTOYCOMA=6, SEPARATOR=7, 
		COMA=8, AMOS=9, FIRSTCHRONICLES=10, SECONDCHRONICLES=11, DANIEL=12, DEUTERONOMY=13, 
		ECCLESIASTES=14, ESTHER=15, EXODUS=16, EZEKIEL=17, EZRA=18, GENESIS=19, 
		HABAKKUK=20, HAGGAI=21, HOSEA=22, ISAIAH=23, JEREMIAH=24, JOB=25, JOEL=26, 
		JONAH=27, JOSHUA=28, JUDGES=29, FIRSTKINGS=30, SECONDKINGS=31, LAMENTATIONS=32, 
		LEVITICUS=33, MALACHI=34, MICAH=35, NAHUM=36, NEHEMIAH=37, NUMBERS=38, 
		OBADIAH=39, PROVERBS=40, PSALMS=41, RUTH=42, FIRSTSAMUEL=43, SECONDSAMUEL=44, 
		SONGOFSOLOMON=45, ZECHARIAH=46, ZEPHANIAH=47, ACTS=48, REVELATION=49, 
		COLOSSIANS=50, FIRSTCORINTHIANS=51, SECONDCORINTHIANS=52, EPHESIANS=53, 
		GALATIANS=54, HEBREWS=55, JAMES=56, JOHN=57, FIRSTJOHN=58, SECONDJOHN=59, 
		THIRDJOHN=60, JUDE=61, LUKE=62, MARK=63, MATTHEW=64, FIRSTPETER=65, SECONDPETER=66, 
		PHILEMON=67, PHILIPPIANS=68, ROMANS=69, FIRSTTHESSALONIANS=70, SECONDTHESSALONIANS=71, 
		FIRSTTIMOTHY=72, SECONDTIMOTHY=73, TITUS=74;
	public static final int
		RULE_ref = 0, RULE_book = 1, RULE_refList = 2, RULE_chapterRef = 3, RULE_simpleRef = 4, 
		RULE_chapterOnly = 5, RULE_chapterVerse = 6, RULE_chapterVerseRange = 7, 
		RULE_rangeRef = 8, RULE_chapterRange = 9, RULE_chapterRangeVerse = 10, 
		RULE_multipleVerseRef = 11, RULE_verseExp = 12;
	public static final String[] ruleNames = {
		"ref", "book", "refList", "chapterRef", "simpleRef", "chapterOnly", "chapterVerse", 
		"chapterVerseRange", "rangeRef", "chapterRange", "chapterRangeVerse", 
		"multipleVerseRef", "verseExp"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, "'.'", "'-'", "':'", null, "';'", null, "','"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "NUM", "DOT", "GUION", "DOSPUNTOS", "SPACE", "PUNTOYCOMA", "SEPARATOR", 
		"COMA", "AMOS", "FIRSTCHRONICLES", "SECONDCHRONICLES", "DANIEL", "DEUTERONOMY", 
		"ECCLESIASTES", "ESTHER", "EXODUS", "EZEKIEL", "EZRA", "GENESIS", "HABAKKUK", 
		"HAGGAI", "HOSEA", "ISAIAH", "JEREMIAH", "JOB", "JOEL", "JONAH", "JOSHUA", 
		"JUDGES", "FIRSTKINGS", "SECONDKINGS", "LAMENTATIONS", "LEVITICUS", "MALACHI", 
		"MICAH", "NAHUM", "NEHEMIAH", "NUMBERS", "OBADIAH", "PROVERBS", "PSALMS", 
		"RUTH", "FIRSTSAMUEL", "SECONDSAMUEL", "SONGOFSOLOMON", "ZECHARIAH", "ZEPHANIAH", 
		"ACTS", "REVELATION", "COLOSSIANS", "FIRSTCORINTHIANS", "SECONDCORINTHIANS", 
		"EPHESIANS", "GALATIANS", "HEBREWS", "JAMES", "JOHN", "FIRSTJOHN", "SECONDJOHN", 
		"THIRDJOHN", "JUDE", "LUKE", "MARK", "MATTHEW", "FIRSTPETER", "SECONDPETER", 
		"PHILEMON", "PHILIPPIANS", "ROMANS", "FIRSTTHESSALONIANS", "SECONDTHESSALONIANS", 
		"FIRSTTIMOTHY", "SECONDTIMOTHY", "TITUS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BibleReference.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BibleReferenceParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class RefContext extends ParserRuleContext {
		public BookContext book() {
			return getRuleContext(BookContext.class,0);
		}
		public RefListContext refList() {
			return getRuleContext(RefListContext.class,0);
		}
		public TerminalNode DOT() { return getToken(BibleReferenceParser.DOT, 0); }
		public List<TerminalNode> SPACE() { return getTokens(BibleReferenceParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(BibleReferenceParser.SPACE, i);
		}
		public RefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ref; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitRef(this);
		}
	}

	public final RefContext ref() throws RecognitionException {
		RefContext _localctx = new RefContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_ref);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26);
			book();
			setState(28);
			_la = _input.LA(1);
			if (_la==DOT) {
				{
				setState(27);
				match(DOT);
				}
			}

			setState(33);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(30);
				match(SPACE);
				}
				}
				setState(35);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(36);
			refList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BookContext extends ParserRuleContext {
		public TerminalNode AMOS() { return getToken(BibleReferenceParser.AMOS, 0); }
		public TerminalNode FIRSTCHRONICLES() { return getToken(BibleReferenceParser.FIRSTCHRONICLES, 0); }
		public TerminalNode SECONDCHRONICLES() { return getToken(BibleReferenceParser.SECONDCHRONICLES, 0); }
		public TerminalNode DANIEL() { return getToken(BibleReferenceParser.DANIEL, 0); }
		public TerminalNode DEUTERONOMY() { return getToken(BibleReferenceParser.DEUTERONOMY, 0); }
		public TerminalNode ECCLESIASTES() { return getToken(BibleReferenceParser.ECCLESIASTES, 0); }
		public TerminalNode ESTHER() { return getToken(BibleReferenceParser.ESTHER, 0); }
		public TerminalNode EXODUS() { return getToken(BibleReferenceParser.EXODUS, 0); }
		public TerminalNode EZEKIEL() { return getToken(BibleReferenceParser.EZEKIEL, 0); }
		public TerminalNode EZRA() { return getToken(BibleReferenceParser.EZRA, 0); }
		public TerminalNode GENESIS() { return getToken(BibleReferenceParser.GENESIS, 0); }
		public TerminalNode HABAKKUK() { return getToken(BibleReferenceParser.HABAKKUK, 0); }
		public TerminalNode HAGGAI() { return getToken(BibleReferenceParser.HAGGAI, 0); }
		public TerminalNode HOSEA() { return getToken(BibleReferenceParser.HOSEA, 0); }
		public TerminalNode ISAIAH() { return getToken(BibleReferenceParser.ISAIAH, 0); }
		public TerminalNode JEREMIAH() { return getToken(BibleReferenceParser.JEREMIAH, 0); }
		public TerminalNode JOB() { return getToken(BibleReferenceParser.JOB, 0); }
		public TerminalNode JOEL() { return getToken(BibleReferenceParser.JOEL, 0); }
		public TerminalNode JONAH() { return getToken(BibleReferenceParser.JONAH, 0); }
		public TerminalNode JOSHUA() { return getToken(BibleReferenceParser.JOSHUA, 0); }
		public TerminalNode JUDGES() { return getToken(BibleReferenceParser.JUDGES, 0); }
		public TerminalNode FIRSTKINGS() { return getToken(BibleReferenceParser.FIRSTKINGS, 0); }
		public TerminalNode SECONDKINGS() { return getToken(BibleReferenceParser.SECONDKINGS, 0); }
		public TerminalNode LAMENTATIONS() { return getToken(BibleReferenceParser.LAMENTATIONS, 0); }
		public TerminalNode LEVITICUS() { return getToken(BibleReferenceParser.LEVITICUS, 0); }
		public TerminalNode MALACHI() { return getToken(BibleReferenceParser.MALACHI, 0); }
		public TerminalNode MICAH() { return getToken(BibleReferenceParser.MICAH, 0); }
		public TerminalNode NAHUM() { return getToken(BibleReferenceParser.NAHUM, 0); }
		public TerminalNode NEHEMIAH() { return getToken(BibleReferenceParser.NEHEMIAH, 0); }
		public TerminalNode NUMBERS() { return getToken(BibleReferenceParser.NUMBERS, 0); }
		public TerminalNode OBADIAH() { return getToken(BibleReferenceParser.OBADIAH, 0); }
		public TerminalNode PROVERBS() { return getToken(BibleReferenceParser.PROVERBS, 0); }
		public TerminalNode PSALMS() { return getToken(BibleReferenceParser.PSALMS, 0); }
		public TerminalNode RUTH() { return getToken(BibleReferenceParser.RUTH, 0); }
		public TerminalNode FIRSTSAMUEL() { return getToken(BibleReferenceParser.FIRSTSAMUEL, 0); }
		public TerminalNode SECONDSAMUEL() { return getToken(BibleReferenceParser.SECONDSAMUEL, 0); }
		public TerminalNode SONGOFSOLOMON() { return getToken(BibleReferenceParser.SONGOFSOLOMON, 0); }
		public TerminalNode ZECHARIAH() { return getToken(BibleReferenceParser.ZECHARIAH, 0); }
		public TerminalNode ZEPHANIAH() { return getToken(BibleReferenceParser.ZEPHANIAH, 0); }
		public TerminalNode ACTS() { return getToken(BibleReferenceParser.ACTS, 0); }
		public TerminalNode REVELATION() { return getToken(BibleReferenceParser.REVELATION, 0); }
		public TerminalNode COLOSSIANS() { return getToken(BibleReferenceParser.COLOSSIANS, 0); }
		public TerminalNode FIRSTCORINTHIANS() { return getToken(BibleReferenceParser.FIRSTCORINTHIANS, 0); }
		public TerminalNode SECONDCORINTHIANS() { return getToken(BibleReferenceParser.SECONDCORINTHIANS, 0); }
		public TerminalNode EPHESIANS() { return getToken(BibleReferenceParser.EPHESIANS, 0); }
		public TerminalNode GALATIANS() { return getToken(BibleReferenceParser.GALATIANS, 0); }
		public TerminalNode HEBREWS() { return getToken(BibleReferenceParser.HEBREWS, 0); }
		public TerminalNode JAMES() { return getToken(BibleReferenceParser.JAMES, 0); }
		public TerminalNode JOHN() { return getToken(BibleReferenceParser.JOHN, 0); }
		public TerminalNode FIRSTJOHN() { return getToken(BibleReferenceParser.FIRSTJOHN, 0); }
		public TerminalNode SECONDJOHN() { return getToken(BibleReferenceParser.SECONDJOHN, 0); }
		public TerminalNode THIRDJOHN() { return getToken(BibleReferenceParser.THIRDJOHN, 0); }
		public TerminalNode JUDE() { return getToken(BibleReferenceParser.JUDE, 0); }
		public TerminalNode LUKE() { return getToken(BibleReferenceParser.LUKE, 0); }
		public TerminalNode MARK() { return getToken(BibleReferenceParser.MARK, 0); }
		public TerminalNode MATTHEW() { return getToken(BibleReferenceParser.MATTHEW, 0); }
		public TerminalNode FIRSTPETER() { return getToken(BibleReferenceParser.FIRSTPETER, 0); }
		public TerminalNode SECONDPETER() { return getToken(BibleReferenceParser.SECONDPETER, 0); }
		public TerminalNode PHILEMON() { return getToken(BibleReferenceParser.PHILEMON, 0); }
		public TerminalNode PHILIPPIANS() { return getToken(BibleReferenceParser.PHILIPPIANS, 0); }
		public TerminalNode ROMANS() { return getToken(BibleReferenceParser.ROMANS, 0); }
		public TerminalNode FIRSTTHESSALONIANS() { return getToken(BibleReferenceParser.FIRSTTHESSALONIANS, 0); }
		public TerminalNode SECONDTHESSALONIANS() { return getToken(BibleReferenceParser.SECONDTHESSALONIANS, 0); }
		public TerminalNode FIRSTTIMOTHY() { return getToken(BibleReferenceParser.FIRSTTIMOTHY, 0); }
		public TerminalNode SECONDTIMOTHY() { return getToken(BibleReferenceParser.SECONDTIMOTHY, 0); }
		public TerminalNode TITUS() { return getToken(BibleReferenceParser.TITUS, 0); }
		public BookContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_book; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterBook(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitBook(this);
		}
	}

	public final BookContext book() throws RecognitionException {
		BookContext _localctx = new BookContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_book);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(38);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << AMOS) | (1L << FIRSTCHRONICLES) | (1L << SECONDCHRONICLES) | (1L << DANIEL) | (1L << DEUTERONOMY) | (1L << ECCLESIASTES) | (1L << ESTHER) | (1L << EXODUS) | (1L << EZEKIEL) | (1L << EZRA) | (1L << GENESIS) | (1L << HABAKKUK) | (1L << HAGGAI) | (1L << HOSEA) | (1L << ISAIAH) | (1L << JEREMIAH) | (1L << JOB) | (1L << JOEL) | (1L << JONAH) | (1L << JOSHUA) | (1L << JUDGES) | (1L << FIRSTKINGS) | (1L << SECONDKINGS) | (1L << LAMENTATIONS) | (1L << LEVITICUS) | (1L << MALACHI) | (1L << MICAH) | (1L << NAHUM) | (1L << NEHEMIAH) | (1L << NUMBERS) | (1L << OBADIAH) | (1L << PROVERBS) | (1L << PSALMS) | (1L << RUTH) | (1L << FIRSTSAMUEL) | (1L << SECONDSAMUEL) | (1L << SONGOFSOLOMON) | (1L << ZECHARIAH) | (1L << ZEPHANIAH) | (1L << ACTS) | (1L << REVELATION) | (1L << COLOSSIANS) | (1L << FIRSTCORINTHIANS) | (1L << SECONDCORINTHIANS) | (1L << EPHESIANS) | (1L << GALATIANS) | (1L << HEBREWS) | (1L << JAMES) | (1L << JOHN) | (1L << FIRSTJOHN) | (1L << SECONDJOHN) | (1L << THIRDJOHN) | (1L << JUDE) | (1L << LUKE) | (1L << MARK))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (MATTHEW - 64)) | (1L << (FIRSTPETER - 64)) | (1L << (SECONDPETER - 64)) | (1L << (PHILEMON - 64)) | (1L << (PHILIPPIANS - 64)) | (1L << (ROMANS - 64)) | (1L << (FIRSTTHESSALONIANS - 64)) | (1L << (SECONDTHESSALONIANS - 64)) | (1L << (FIRSTTIMOTHY - 64)) | (1L << (SECONDTIMOTHY - 64)) | (1L << (TITUS - 64)))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RefListContext extends ParserRuleContext {
		public List<ChapterRefContext> chapterRef() {
			return getRuleContexts(ChapterRefContext.class);
		}
		public ChapterRefContext chapterRef(int i) {
			return getRuleContext(ChapterRefContext.class,i);
		}
		public List<TerminalNode> PUNTOYCOMA() { return getTokens(BibleReferenceParser.PUNTOYCOMA); }
		public TerminalNode PUNTOYCOMA(int i) {
			return getToken(BibleReferenceParser.PUNTOYCOMA, i);
		}
		public List<TerminalNode> SPACE() { return getTokens(BibleReferenceParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(BibleReferenceParser.SPACE, i);
		}
		public RefListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_refList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterRefList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitRefList(this);
		}
	}

	public final RefListContext refList() throws RecognitionException {
		RefListContext _localctx = new RefListContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_refList);
		int _la;
		try {
			setState(61);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(40);
				chapterRef();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(41);
				chapterRef();
				setState(58);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==SPACE || _la==PUNTOYCOMA) {
					{
					{
					setState(45);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==SPACE) {
						{
						{
						setState(42);
						match(SPACE);
						}
						}
						setState(47);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(48);
					match(PUNTOYCOMA);
					setState(52);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==SPACE) {
						{
						{
						setState(49);
						match(SPACE);
						}
						}
						setState(54);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(55);
					chapterRef();
					}
					}
					setState(60);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterRefContext extends ParserRuleContext {
		public SimpleRefContext simpleRef() {
			return getRuleContext(SimpleRefContext.class,0);
		}
		public RangeRefContext rangeRef() {
			return getRuleContext(RangeRefContext.class,0);
		}
		public MultipleVerseRefContext multipleVerseRef() {
			return getRuleContext(MultipleVerseRefContext.class,0);
		}
		public ChapterRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterRef(this);
		}
	}

	public final ChapterRefContext chapterRef() throws RecognitionException {
		ChapterRefContext _localctx = new ChapterRefContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_chapterRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				{
				setState(63);
				simpleRef();
				}
				break;
			case 2:
				{
				setState(64);
				rangeRef();
				}
				break;
			case 3:
				{
				setState(65);
				multipleVerseRef();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleRefContext extends ParserRuleContext {
		public ChapterOnlyContext chapterOnly() {
			return getRuleContext(ChapterOnlyContext.class,0);
		}
		public ChapterVerseContext chapterVerse() {
			return getRuleContext(ChapterVerseContext.class,0);
		}
		public ChapterVerseRangeContext chapterVerseRange() {
			return getRuleContext(ChapterVerseRangeContext.class,0);
		}
		public SimpleRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterSimpleRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitSimpleRef(this);
		}
	}

	public final SimpleRefContext simpleRef() throws RecognitionException {
		SimpleRefContext _localctx = new SimpleRefContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_simpleRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				{
				setState(68);
				chapterOnly();
				}
				break;
			case 2:
				{
				setState(69);
				chapterVerse();
				}
				break;
			case 3:
				{
				setState(70);
				chapterVerseRange();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterOnlyContext extends ParserRuleContext {
		public TerminalNode NUM() { return getToken(BibleReferenceParser.NUM, 0); }
		public ChapterOnlyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterOnly; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterOnly(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterOnly(this);
		}
	}

	public final ChapterOnlyContext chapterOnly() throws RecognitionException {
		ChapterOnlyContext _localctx = new ChapterOnlyContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_chapterOnly);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			match(NUM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterVerseContext extends ParserRuleContext {
		public List<TerminalNode> NUM() { return getTokens(BibleReferenceParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(BibleReferenceParser.NUM, i);
		}
		public TerminalNode DOT() { return getToken(BibleReferenceParser.DOT, 0); }
		public List<TerminalNode> SPACE() { return getTokens(BibleReferenceParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(BibleReferenceParser.SPACE, i);
		}
		public ChapterVerseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterVerse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterVerse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterVerse(this);
		}
	}

	public final ChapterVerseContext chapterVerse() throws RecognitionException {
		ChapterVerseContext _localctx = new ChapterVerseContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_chapterVerse);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			match(NUM);
			setState(83);
			switch (_input.LA(1)) {
			case DOSPUNTOS:
				{
				setState(76);
				match(DOSPUNTOS);
				}
				break;
			case DOT:
				{
				setState(77);
				match(DOT);
				}
				break;
			case SPACE:
				{
				setState(79); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(78);
					match(SPACE);
					}
					}
					setState(81); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==SPACE );
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(85);
			match(NUM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterVerseRangeContext extends ParserRuleContext {
		public List<TerminalNode> NUM() { return getTokens(BibleReferenceParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(BibleReferenceParser.NUM, i);
		}
		public TerminalNode DOT() { return getToken(BibleReferenceParser.DOT, 0); }
		public ChapterVerseRangeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterVerseRange; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterVerseRange(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterVerseRange(this);
		}
	}

	public final ChapterVerseRangeContext chapterVerseRange() throws RecognitionException {
		ChapterVerseRangeContext _localctx = new ChapterVerseRangeContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_chapterVerseRange);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			match(NUM);
			setState(88);
			_la = _input.LA(1);
			if ( !(_la==DOT || _la==DOSPUNTOS) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(89);
			match(NUM);
			setState(90);
			match(GUION);
			setState(91);
			match(NUM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RangeRefContext extends ParserRuleContext {
		public ChapterRangeContext chapterRange() {
			return getRuleContext(ChapterRangeContext.class,0);
		}
		public ChapterRangeVerseContext chapterRangeVerse() {
			return getRuleContext(ChapterRangeVerseContext.class,0);
		}
		public RangeRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rangeRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterRangeRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitRangeRef(this);
		}
	}

	public final RangeRefContext rangeRef() throws RecognitionException {
		RangeRefContext _localctx = new RangeRefContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_rangeRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(95);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				{
				setState(93);
				chapterRange();
				}
				break;
			case 2:
				{
				setState(94);
				chapterRangeVerse();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterRangeContext extends ParserRuleContext {
		public List<TerminalNode> NUM() { return getTokens(BibleReferenceParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(BibleReferenceParser.NUM, i);
		}
		public List<TerminalNode> SPACE() { return getTokens(BibleReferenceParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(BibleReferenceParser.SPACE, i);
		}
		public ChapterRangeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterRange; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterRange(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterRange(this);
		}
	}

	public final ChapterRangeContext chapterRange() throws RecognitionException {
		ChapterRangeContext _localctx = new ChapterRangeContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_chapterRange);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			match(NUM);
			setState(101);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(98);
				match(SPACE);
				}
				}
				setState(103);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(104);
			match(GUION);
			setState(108);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(105);
				match(SPACE);
				}
				}
				setState(110);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(111);
			match(NUM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChapterRangeVerseContext extends ParserRuleContext {
		public List<TerminalNode> NUM() { return getTokens(BibleReferenceParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(BibleReferenceParser.NUM, i);
		}
		public List<TerminalNode> DOT() { return getTokens(BibleReferenceParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(BibleReferenceParser.DOT, i);
		}
		public List<TerminalNode> SPACE() { return getTokens(BibleReferenceParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(BibleReferenceParser.SPACE, i);
		}
		public ChapterRangeVerseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chapterRangeVerse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterChapterRangeVerse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitChapterRangeVerse(this);
		}
	}

	public final ChapterRangeVerseContext chapterRangeVerse() throws RecognitionException {
		ChapterRangeVerseContext _localctx = new ChapterRangeVerseContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_chapterRangeVerse);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			match(NUM);
			setState(128);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				{
				setState(117);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==SPACE) {
					{
					{
					setState(114);
					match(SPACE);
					}
					}
					setState(119);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(120);
				_la = _input.LA(1);
				if ( !(_la==DOT || _la==DOSPUNTOS) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(124);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==SPACE) {
					{
					{
					setState(121);
					match(SPACE);
					}
					}
					setState(126);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(127);
				match(NUM);
				}
				break;
			}
			setState(133);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(130);
				match(SPACE);
				}
				}
				setState(135);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(136);
			match(GUION);
			setState(140);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(137);
				match(SPACE);
				}
				}
				setState(142);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(143);
			match(NUM);
			setState(144);
			_la = _input.LA(1);
			if ( !(_la==DOT || _la==DOSPUNTOS) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(148);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(145);
				match(SPACE);
				}
				}
				setState(150);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(151);
			match(NUM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultipleVerseRefContext extends ParserRuleContext {
		public TerminalNode NUM() { return getToken(BibleReferenceParser.NUM, 0); }
		public List<VerseExpContext> verseExp() {
			return getRuleContexts(VerseExpContext.class);
		}
		public VerseExpContext verseExp(int i) {
			return getRuleContext(VerseExpContext.class,i);
		}
		public TerminalNode DOT() { return getToken(BibleReferenceParser.DOT, 0); }
		public List<TerminalNode> SPACE() { return getTokens(BibleReferenceParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(BibleReferenceParser.SPACE, i);
		}
		public MultipleVerseRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multipleVerseRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterMultipleVerseRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitMultipleVerseRef(this);
		}
	}

	public final MultipleVerseRefContext multipleVerseRef() throws RecognitionException {
		MultipleVerseRefContext _localctx = new MultipleVerseRefContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_multipleVerseRef);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(153);
			match(NUM);
			setState(157);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(154);
				match(SPACE);
				}
				}
				setState(159);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(160);
			_la = _input.LA(1);
			if ( !(_la==DOT || _la==DOSPUNTOS) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			setState(164);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(161);
				match(SPACE);
				}
				}
				setState(166);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(167);
			verseExp();
			setState(184);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(171);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==SPACE) {
						{
						{
						setState(168);
						match(SPACE);
						}
						}
						setState(173);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(174);
					match(COMA);
					setState(178);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==SPACE) {
						{
						{
						setState(175);
						match(SPACE);
						}
						}
						setState(180);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(181);
					verseExp();
					}
					} 
				}
				setState(186);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VerseExpContext extends ParserRuleContext {
		public List<TerminalNode> NUM() { return getTokens(BibleReferenceParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(BibleReferenceParser.NUM, i);
		}
		public List<TerminalNode> SPACE() { return getTokens(BibleReferenceParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(BibleReferenceParser.SPACE, i);
		}
		public VerseExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_verseExp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).enterVerseExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BibleReferenceListener ) ((BibleReferenceListener)listener).exitVerseExp(this);
		}
	}

	public final VerseExpContext verseExp() throws RecognitionException {
		VerseExpContext _localctx = new VerseExpContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_verseExp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(203);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				{
				setState(187);
				match(NUM);
				}
				break;
			case 2:
				{
				{
				setState(188);
				match(NUM);
				setState(192);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==SPACE) {
					{
					{
					setState(189);
					match(SPACE);
					}
					}
					setState(194);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(195);
				match(GUION);
				setState(199);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==SPACE) {
					{
					{
					setState(196);
					match(SPACE);
					}
					}
					setState(201);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(202);
				match(NUM);
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3L\u00d0\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\5\2\37\n\2\3\2\7\2\"\n\2\f\2\16"+
		"\2%\13\2\3\2\3\2\3\3\3\3\3\4\3\4\3\4\7\4.\n\4\f\4\16\4\61\13\4\3\4\3\4"+
		"\7\4\65\n\4\f\4\16\48\13\4\3\4\7\4;\n\4\f\4\16\4>\13\4\5\4@\n\4\3\5\3"+
		"\5\3\5\5\5E\n\5\3\6\3\6\3\6\5\6J\n\6\3\7\3\7\3\b\3\b\3\b\3\b\6\bR\n\b"+
		"\r\b\16\bS\5\bV\n\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\5\nb\n\n\3"+
		"\13\3\13\7\13f\n\13\f\13\16\13i\13\13\3\13\3\13\7\13m\n\13\f\13\16\13"+
		"p\13\13\3\13\3\13\3\f\3\f\7\fv\n\f\f\f\16\fy\13\f\3\f\3\f\7\f}\n\f\f\f"+
		"\16\f\u0080\13\f\3\f\5\f\u0083\n\f\3\f\7\f\u0086\n\f\f\f\16\f\u0089\13"+
		"\f\3\f\3\f\7\f\u008d\n\f\f\f\16\f\u0090\13\f\3\f\3\f\3\f\7\f\u0095\n\f"+
		"\f\f\16\f\u0098\13\f\3\f\3\f\3\r\3\r\7\r\u009e\n\r\f\r\16\r\u00a1\13\r"+
		"\3\r\3\r\7\r\u00a5\n\r\f\r\16\r\u00a8\13\r\3\r\3\r\7\r\u00ac\n\r\f\r\16"+
		"\r\u00af\13\r\3\r\3\r\7\r\u00b3\n\r\f\r\16\r\u00b6\13\r\3\r\7\r\u00b9"+
		"\n\r\f\r\16\r\u00bc\13\r\3\16\3\16\3\16\7\16\u00c1\n\16\f\16\16\16\u00c4"+
		"\13\16\3\16\3\16\7\16\u00c8\n\16\f\16\16\16\u00cb\13\16\3\16\5\16\u00ce"+
		"\n\16\3\16\2\2\17\2\4\6\b\n\f\16\20\22\24\26\30\32\2\4\3\2\13L\4\2\4\4"+
		"\6\6\u00e0\2\34\3\2\2\2\4(\3\2\2\2\6?\3\2\2\2\bD\3\2\2\2\nI\3\2\2\2\f"+
		"K\3\2\2\2\16M\3\2\2\2\20Y\3\2\2\2\22a\3\2\2\2\24c\3\2\2\2\26s\3\2\2\2"+
		"\30\u009b\3\2\2\2\32\u00cd\3\2\2\2\34\36\5\4\3\2\35\37\7\4\2\2\36\35\3"+
		"\2\2\2\36\37\3\2\2\2\37#\3\2\2\2 \"\7\7\2\2! \3\2\2\2\"%\3\2\2\2#!\3\2"+
		"\2\2#$\3\2\2\2$&\3\2\2\2%#\3\2\2\2&\'\5\6\4\2\'\3\3\2\2\2()\t\2\2\2)\5"+
		"\3\2\2\2*@\5\b\5\2+<\5\b\5\2,.\7\7\2\2-,\3\2\2\2.\61\3\2\2\2/-\3\2\2\2"+
		"/\60\3\2\2\2\60\62\3\2\2\2\61/\3\2\2\2\62\66\7\b\2\2\63\65\7\7\2\2\64"+
		"\63\3\2\2\2\658\3\2\2\2\66\64\3\2\2\2\66\67\3\2\2\2\679\3\2\2\28\66\3"+
		"\2\2\29;\5\b\5\2:/\3\2\2\2;>\3\2\2\2<:\3\2\2\2<=\3\2\2\2=@\3\2\2\2><\3"+
		"\2\2\2?*\3\2\2\2?+\3\2\2\2@\7\3\2\2\2AE\5\n\6\2BE\5\22\n\2CE\5\30\r\2"+
		"DA\3\2\2\2DB\3\2\2\2DC\3\2\2\2E\t\3\2\2\2FJ\5\f\7\2GJ\5\16\b\2HJ\5\20"+
		"\t\2IF\3\2\2\2IG\3\2\2\2IH\3\2\2\2J\13\3\2\2\2KL\7\3\2\2L\r\3\2\2\2MU"+
		"\7\3\2\2NV\7\6\2\2OV\7\4\2\2PR\7\7\2\2QP\3\2\2\2RS\3\2\2\2SQ\3\2\2\2S"+
		"T\3\2\2\2TV\3\2\2\2UN\3\2\2\2UO\3\2\2\2UQ\3\2\2\2VW\3\2\2\2WX\7\3\2\2"+
		"X\17\3\2\2\2YZ\7\3\2\2Z[\t\3\2\2[\\\7\3\2\2\\]\7\5\2\2]^\7\3\2\2^\21\3"+
		"\2\2\2_b\5\24\13\2`b\5\26\f\2a_\3\2\2\2a`\3\2\2\2b\23\3\2\2\2cg\7\3\2"+
		"\2df\7\7\2\2ed\3\2\2\2fi\3\2\2\2ge\3\2\2\2gh\3\2\2\2hj\3\2\2\2ig\3\2\2"+
		"\2jn\7\5\2\2km\7\7\2\2lk\3\2\2\2mp\3\2\2\2nl\3\2\2\2no\3\2\2\2oq\3\2\2"+
		"\2pn\3\2\2\2qr\7\3\2\2r\25\3\2\2\2s\u0082\7\3\2\2tv\7\7\2\2ut\3\2\2\2"+
		"vy\3\2\2\2wu\3\2\2\2wx\3\2\2\2xz\3\2\2\2yw\3\2\2\2z~\t\3\2\2{}\7\7\2\2"+
		"|{\3\2\2\2}\u0080\3\2\2\2~|\3\2\2\2~\177\3\2\2\2\177\u0081\3\2\2\2\u0080"+
		"~\3\2\2\2\u0081\u0083\7\3\2\2\u0082w\3\2\2\2\u0082\u0083\3\2\2\2\u0083"+
		"\u0087\3\2\2\2\u0084\u0086\7\7\2\2\u0085\u0084\3\2\2\2\u0086\u0089\3\2"+
		"\2\2\u0087\u0085\3\2\2\2\u0087\u0088\3\2\2\2\u0088\u008a\3\2\2\2\u0089"+
		"\u0087\3\2\2\2\u008a\u008e\7\5\2\2\u008b\u008d\7\7\2\2\u008c\u008b\3\2"+
		"\2\2\u008d\u0090\3\2\2\2\u008e\u008c\3\2\2\2\u008e\u008f\3\2\2\2\u008f"+
		"\u0091\3\2\2\2\u0090\u008e\3\2\2\2\u0091\u0092\7\3\2\2\u0092\u0096\t\3"+
		"\2\2\u0093\u0095\7\7\2\2\u0094\u0093\3\2\2\2\u0095\u0098\3\2\2\2\u0096"+
		"\u0094\3\2\2\2\u0096\u0097\3\2\2\2\u0097\u0099\3\2\2\2\u0098\u0096\3\2"+
		"\2\2\u0099\u009a\7\3\2\2\u009a\27\3\2\2\2\u009b\u009f\7\3\2\2\u009c\u009e"+
		"\7\7\2\2\u009d\u009c\3\2\2\2\u009e\u00a1\3\2\2\2\u009f\u009d\3\2\2\2\u009f"+
		"\u00a0\3\2\2\2\u00a0\u00a2\3\2\2\2\u00a1\u009f\3\2\2\2\u00a2\u00a6\t\3"+
		"\2\2\u00a3\u00a5\7\7\2\2\u00a4\u00a3\3\2\2\2\u00a5\u00a8\3\2\2\2\u00a6"+
		"\u00a4\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a9\3\2\2\2\u00a8\u00a6\3\2"+
		"\2\2\u00a9\u00ba\5\32\16\2\u00aa\u00ac\7\7\2\2\u00ab\u00aa\3\2\2\2\u00ac"+
		"\u00af\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\u00b0\3\2"+
		"\2\2\u00af\u00ad\3\2\2\2\u00b0\u00b4\7\n\2\2\u00b1\u00b3\7\7\2\2\u00b2"+
		"\u00b1\3\2\2\2\u00b3\u00b6\3\2\2\2\u00b4\u00b2\3\2\2\2\u00b4\u00b5\3\2"+
		"\2\2\u00b5\u00b7\3\2\2\2\u00b6\u00b4\3\2\2\2\u00b7\u00b9\5\32\16\2\u00b8"+
		"\u00ad\3\2\2\2\u00b9\u00bc\3\2\2\2\u00ba\u00b8\3\2\2\2\u00ba\u00bb\3\2"+
		"\2\2\u00bb\31\3\2\2\2\u00bc\u00ba\3\2\2\2\u00bd\u00ce\7\3\2\2\u00be\u00c2"+
		"\7\3\2\2\u00bf\u00c1\7\7\2\2\u00c0\u00bf\3\2\2\2\u00c1\u00c4\3\2\2\2\u00c2"+
		"\u00c0\3\2\2\2\u00c2\u00c3\3\2\2\2\u00c3\u00c5\3\2\2\2\u00c4\u00c2\3\2"+
		"\2\2\u00c5\u00c9\7\5\2\2\u00c6\u00c8\7\7\2\2\u00c7\u00c6\3\2\2\2\u00c8"+
		"\u00cb\3\2\2\2\u00c9\u00c7\3\2\2\2\u00c9\u00ca\3\2\2\2\u00ca\u00cc\3\2"+
		"\2\2\u00cb\u00c9\3\2\2\2\u00cc\u00ce\7\3\2\2\u00cd\u00bd\3\2\2\2\u00cd"+
		"\u00be\3\2\2\2\u00ce\33\3\2\2\2\35\36#/\66<?DISUagnw~\u0082\u0087\u008e"+
		"\u0096\u009f\u00a6\u00ad\u00b4\u00ba\u00c2\u00c9\u00cd";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}