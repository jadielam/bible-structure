package com.ibibl.bible;

import java.io.PrintWriter;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import com.ibibl.bible.parser.BibleWorksFormatParser;

public class BibleOutlineCreator {

	public static void main(String [] args) throws Exception{
		
		String inputFile = "/home/jadiel/Documents/Bibles/rva.txt";
		BibleVersion version = BibleVersion.RVA;
		String outputFile = "RVABibleOutline.java";
		
		//1. Read the BibleWorks format into a list of references.
		List<BibleReference> references = BibleWorksFormatParser.getReferences(inputFile, version);
		
		//2. Use the list of references to create the source code needed.
		String sourceCode = createBibleOutline(references, "RVABibleOutline", version);
		System.out.println(sourceCode.charAt(sourceCode.length()-1));
		
		//3. Right the source code to a file.
		PrintWriter out = new PrintWriter(outputFile);
		out.println(sourceCode);
		out.flush();
		out.close();
		
	}
	
	private static String createBibleOutline(List<BibleReference> references, String className,
			BibleVersion version){
		
		StringBuilder outline = new StringBuilder();
		
		//0. The package and import declarations
		outline.append("package com.ibibl.bible;\n\n");
		outline.append("import java.util.List;\n");
		outline.append("import java.util.ArrayList;\n");
				
		outline.append("\n");
		
		
		//1. The header of the class
		outline.append("public class ");
		outline.append(className);
		outline.append(" extends BibleOutline {\n\n");
		
		//2. The fields of the class
		//2.1 Opening the string field
		//2.2 Initialize outlines
		outline.append("\tprivate static "+className+" instance;\n\n");
		StringBuilder [] outlines = new StringBuilder[8];
		for (int i = 0; i < outlines.length; ++i){
			outlines[i] = new StringBuilder();
			outlines[i].append("\tpublic static final String BIBLE_OUTLINE"
					+ Integer.toString(i+1)+" = ");
			outlines[i].append("\"");
		}
		
		
		int counter = 1;
		int totalReferences = references.size();
		int chunk = totalReferences/7;
				
		for (BibleReference ref : references){
			StringBuilder outlineToUse = null;
			if (counter > chunk * 7){
				outlineToUse = outlines[7];
				if (counter == chunk *7 +1){
					outlines[6].append("\";");
					outlines[6].append("\n\n\n");
				}
				
			}
			else if (counter > chunk * 6){
				outlineToUse = outlines[6];
				if (counter == chunk * 6 +1){
					outlines[5].append("\";");
					outlines[5].append("\n\n\n");
				}
				
			}
			else if (counter > chunk * 5){
				outlineToUse = outlines[5];
				if (counter == chunk * 5 + 1){
					outlines[4].append("\";");
					outlines[4].append("\n\n\n");
				}
				
			}
			else if (counter > chunk * 4){
				outlineToUse = outlines[4];
				if (counter == chunk * 4 + 1){
					outlines[3].append("\";");
					outlines[3].append("\n\n\n");
				}
				
			}
			else if (counter > chunk * 3){
				outlineToUse = outlines[3];
				if (counter == chunk * 3 + 1){
					outlines[2].append("\";");
					outlines[2].append("\n\n\n");
				}
				
			}
			else if (counter > chunk * 2){
				outlineToUse = outlines[2];
				if (counter == chunk * 2 + 1){
					outlines[1].append("\";");
					outlines[1].append("\n\n\n");
				}
				
			}
			else if (counter > chunk *1){
				outlineToUse = outlines[1];
				if (counter == chunk * 1 + 1){
					outlines[0].append("\";");
					outlines[0].append("\n\n\n");
				}
				
			}
			else{
				outlineToUse = outlines[0];
			}
			
			Book book = ref.getBook();
			Integer chapter = ref.getChapter();
			Integer verse = ref.getVerse();
			if (counter % 5 == 0){
				outlineToUse.append("\"+\n");
				outlineToUse.append("\t\t\t\t\t\"");
			}
						
			outlineToUse.append(book.getOrder());
			outlineToUse.append(" ");
			outlineToUse.append(chapter);
			outlineToUse.append(":");
			outlineToUse.append(verse);
			outlineToUse.append("\\n");
			counter++;
		}
		
		outlines[7].append("\";");
		outlines[7].append("\n\n\n");
		
		for (int i = 0; i < outlines.length; ++i){
			outline.append(outlines[i].toString());
		}
		//3. The constructor of the class
		//3.1 Opening constructor
		outline.append("\tpublic ");
		outline.append(className);
		outline.append("() {\n");
		outline.append("\t\tsuper(BibleVersion."+version.name()+");\n");
		outline.append("\t\tList<BibleReference> list = new ArrayList<BibleReference>();\n");
		
		for (int i = 0; i < outlines.length; ++i){
			outline.append("\t\tlist.addAll(super.createListOfReferences(BIBLE_OUTLINE"+
					Integer.toString(i+1)+", BibleVersion."+version.name()+"));\n");
		}
		
		outline.append("\n");
		outline.append("\t\tsuper.createOutline(list);\n");
				
		//3.3 Closing constructor
		outline.append("\t}\n\n");
		
		//3.4 Creating builder
		outline.append("\tpublic static "+className+" getInstance(){\n");
		outline.append("\t\tif (this.instance == null){\n");
		outline.append("\t\t\tthis.instance = new "+className+"();\n");
		outline.append("\t\t}\n");
		outline.append("\t\treturn this.instance;\n");
		outline.append("\t}\n");
		
		
		//4. Closing what was opened
		outline.append("}");
		
		//4. Returning the bible of the class at the end.
		return outline.toString();
	}
	
	
}
