package com.ibibl.bible;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ibibl.bible.parser.BibleReferenceReg;
import com.ibibl.bible.parser.SimpleBibleReferenceReg;

public class Test {

	public static void main (String [] args){
		
		String a = "In the Bible, in Matt. 1 - 4 : 3 we find Matt 3 : 4,45 and in Matt 1 3";
		Pattern pattern = BibleReferenceReg.REF_PATTERN;
		
		Matcher matcher = pattern.matcher(a);
		while (matcher.find()){
			
			System.out.println(matcher.group());
		}
		
		String b = "IN the bible there is Matt 1 1 and Matt. 1:2 and Matt. 1.3";
		Pattern pattern1 = SimpleBibleReferenceReg.REF_PATTERN;
		
		Matcher matcher1 = pattern1.matcher(b);
		while(matcher1.find()){
			System.out.println(matcher1.group());
		}
		
		BibleVersion version = BibleVersion.NIV;
		System.out.println(version.getAbv());
		Book book = Book.SecondThessalonians;
		book.name();
		System.out.println(book.name());
	}
}
