package com.ibibl.bible;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import com.ibibl.bible.parser.BibleReferenceLexer;
import com.ibibl.bible.parser.BibleReferenceListener;
import com.ibibl.bible.parser.BibleReferenceListenerImpl;
import com.ibibl.bible.parser.BibleReferenceParser;
import com.ibibl.bible.parser.BibleWorksFormatParser;


public class TestBibleReferenceParser {

	public static void main(String[] args) throws Exception {
		
		//1. Create the BibleOutline
		
		BibleOutline outline = NIVBibleOutline.getInstance();
		
		//2. Create the examples
		//String [] examples = {"Matt 1 1", "Luk 3 : 1 - 3", "Mark. 1:3 , 4 , 5 , 10-23"};
		String [] examples = {"sdf 1 1"};	
		//3. Get the parsed reference and print it to see if is good.
		for (String example : examples){
			ANTLRInputStream input = new ANTLRInputStream(example);
			BibleReferenceLexer lexer = new BibleReferenceLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			BibleReferenceParser parser = new BibleReferenceParser(tokens);
			ParseTree tree = parser.ref();
			
			ParseTreeWalker walker = new ParseTreeWalker();
			List<Reference> references = new ArrayList<Reference>();
			BibleReferenceListenerImpl listener = new BibleReferenceListenerImpl(outline, references);
			walker.walk(listener, tree);
			
			references = listener.getCollectedReferences();
			System.out.println(references.size());
			for (Reference reference : references){
				System.out.println(reference.toString());
			}
		}
		
		

	}

}
